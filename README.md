# InstaLate
> Online dictionary that doesn't require you to switch apps by hand.


<a href="https://f-droid.org/en/packages/com.concept1tech.instalate/">
    <img alt="Get it on F-Droid" src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="60" align="middle">
</a>

Direct APK download: [Releases](https://gitlab.com/concept1tech/instalate/-/releases)

Source (project uses [submodule](https://gitlab.com/concept1tech/unn)):

    git clone --recurse-submodules https://gitlab.com/concept1tech/instalate.git

# Table of Contents
1. [Features](#features)
2. [Permissions](#permissions)
3. [Privacy](#privacy)
4. [System](#system)
5. [Screenshots](#screenshots)
6. [Tips and Tricks](#tips-and-tricks)
7. [Thanks](#thanks)
8. [FAQ](#faq)


## Features
- Distraction-free translation displayed above the currently used app
- Choose from different translation providers
- 2 different *trigger modes*:
  1. Context menu
  2. Floating button (requires the word to be copied/cut to the system clipboard first)
      - Button freely movable via drag 'n' drop
      - Android versions < 10 only: show/hide button automatically


## Permissions
- **INTERNET**: Allows InstaLate to use the internet to lookup translations
- **ACCESS_NETWORK_STATE**: Allows InstaLate to verify if the device is connected to the internet
- **FOREGROUND_SERVICE**: Allows InstaLate to run a service that listens to clipboard changes and to show a floating button
- **RECEIVE_BOOT_COMPLETED**: Allows InstaLate to optionally start this service during start-up of the device
- **SYSTEM_ALERT_WINDOW**: Allows InstaLate to display a floating button over other apps


## Privacy
- No search history is stored locally or remotely by this app (last search term is cached in memory only) but it may or may not be possible that your search is tracked by the selected translation provider
- No client-side JavaScript is executed
- HTTPS communication to translation providers

*Please be extra cautious to not share sensitive data (passwords etc.) with translation providers by accident! E.g., if you're using the floating button don't trigger a translation if your password has been copied to the system clipboard (apart from that you should never copypaste your passwords).*


## System
- Minimum Version: Android 6.0, "Marshmallow" (API 23)
- Target Version: Android 11 (API 30)


## Screenshots
<a href="fastlane/metadata/android/en-US/images/phoneScreenshots/screencast_a.gif">
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/screencast_a.gif" alt="screencast" width="300"/>
</a>

| | | | | |
|-|-|-|-|-|
|[![](fastlane/metadata/android/en-US/images/phoneScreenshots/1.png)](fastlane/metadata/android/en-US/images/phoneScreenshots/1.png) | [![](fastlane/metadata/android/en-US/images/phoneScreenshots/2.png)](fastlane/metadata/android/en-US/images/phoneScreenshots/2.png) | [![](fastlane/metadata/android/en-US/images/phoneScreenshots/3.png)](fastlane/metadata/android/en-US/images/phoneScreenshots/3.png) | [![](fastlane/metadata/android/en-US/images/phoneScreenshots/4.png)](fastlane/metadata/android/en-US/images/phoneScreenshots/4.png) | [![](fastlane/metadata/android/en-US/images/phoneScreenshots/5.png)](fastlane/metadata/android/en-US/images/phoneScreenshots/5.png)|


## Tips and Tricks
- If an app has implemented a custom context menu see if the floating button trigger mode works for you which utilizes the system clipboard
- Integration into "Librera" (FOSS ebook reader):
   1. Select any word in the document by long-pressing it (text selection must be supported)
   2. In the opening dialog tap the "+" sign at the bottom and select InstaLate
   3. Next time you long-press a word you'll have access to the InstaLate button at the bottom


## Thanks
To all translation providers and their awesome services this application would be nothing without. Thank you for supporting the Open Source Community. In alphabetical order:
- Beolingus: [https://dict.tu-chemnitz.de/](https://dict.tu-chemnitz.de/)
- Dict.[]()cc: [https://www.dict.cc/](https://www.dict.cc/)
- GNU CIDE: [https://gcide.gnu.org.ua/](https://gcide.gnu.org.ua/)
- Linguee: [https://www.linguee.com/](https://www.linguee.com/)
- WikDict: [https://www.wikdict.com/](https://www.wikdict.com/)
- Wiktionary: [https://wiktionary.org/](https://wiktionary.org/)


## FAQ
1. **Are there any plans for an offline dictionary?**
   - No. A least not in the near future.
2. **Can Google Translations be included to the list of providers?**
   - No. For privacy and ethical reasons Google Translations will not be included to the list of providers
