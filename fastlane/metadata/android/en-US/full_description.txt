Features:
- Distraction-free translation displayed above the currently used app
- Choose from different translation providers
- 2 different <i>trigger modes</i>:
  1. Context menu
  2. Floating button (requires the word to be copied/cut to the system clipboard first)
      - Button freely movable via drag 'n' drop
      - Android versions < 10 only: show/hide button automatically

Permissions:
- <b>INTERNET</b>:
  Allows InstaLate to use the internet to lookup translations
- <b>ACCESS_NETWORK_STATE</b>:
  Allows InstaLate to verify if the device is connected to the internet
- <b>FOREGROUND_SERVICE</b>:
  Allows InstaLate to run a service that listens to clipboard changes and to show a floating button
- <b>RECEIVE_BOOT_COMPLETED</b>:
  Allows InstaLate to optionally start this service during start-up of the device
- <b>SYSTEM_ALERT_WINDOW</b>:
  Allows InstaLate to display a floating button over other apps

Privacy:
- No search history is stored locally or remotely by this app (last search term is cached in memory only) but it may or may not be possible that your search is tracked by the selected translation provider
- No client-side JavaScript is executed
- HTTPS communication to translation providers

<b>Please be extra cautious to not share sensitive data (passwords etc.) with translation providers by accident! E.g., if you're using the floating button don't trigger a translation if your password has been copied to the system clipboard (apart from that you should never copypaste your passwords).</b>

Tips and Tricks:
- If an app has implemented a custom context menu see if the floating button trigger mode works for you which utilizes the system clipboard
- Integration into "Librera" (FOSS ebook reader):
   1. Select any word in the document by long-pressing it (text selection must be supported)
   2. In the opening dialog tap the "+" sign at the bottom and select InstaLate
   3. Next time you long-press a word you'll have access to the InstaLate button at the bottom

Thanks:
To all translation providers and their awesome services this application would be nothing without (in alphabetical order):
- Beolingus: https://dict.tu-chemnitz.de/
- Dict.[]()cc: https://www.dict.cc/
- GNU CIDE: https://gcide.gnu.org.ua/
- Linguee: https://www.linguee.com/
- WikDict: https://www.wikdict.com/
- Wiktionary: https://wiktionary.org/
