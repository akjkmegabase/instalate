/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import com.concept1tech.unn.DroidUtils;

public class TriggerView extends FrameLayout {

    public static final String TAG = TriggerView.class.getSimpleName();
    private static final int ANIM_DURATION = 200;
    private static final float ANIM_SIZE = 1.25f;
    // if TYPE_TOAST doesn't work for higher APIs try: TYPE_PHONE, TYPE_APPLICATION_OVERLAY, TYPE_APPLICATION_PANEL or TYPE_SYSTEM_ALERT (in that order)
    private static final int TYPE_FLAG = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ?
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY : // needs ACTION_MANAGE_OVERLAY_PERMISSION granted since API23/M
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;   // deprecated since API26/O

    private final PointF mOffset = new PointF();
    private final PointF mInScreen = new PointF();
    private boolean mDragActive;
    ImageView mTriggerbutton;
    private Context mCtx;
    private WindowManager mWindowManager;
    private WindowManager.LayoutParams mLayoutParams;
    private ScaleAnimation mScaleUpAnim;
    private ScaleAnimation mScaleDownAnim;
    private OnNewDropPositionListener mOnNewDropPositionListener;
    private OnNewRelativeDropPositionListener mOnNewRelativeDropPositionListener;
    private Position mPosition;
    private final Point mLastDropPos = new Point(-0xfeedcafe, -0xfeedcafe);
    private final PointF mLastRelDropPos = new PointF(-0xfeedcafe, -0xfeedcafe);
    private final Handler mHandler = new Handler();

    public TriggerView(Context context) {
        super(context);
        setUp(context);
    }

    public TriggerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUp(context);
    }
    public TriggerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUp(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TriggerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setUp(context);
    }

    private void setUp(Context c) {
        mCtx = c;
        mWindowManager = (WindowManager) mCtx.getSystemService(Context.WINDOW_SERVICE);

        mLayoutParams = newLayoutParams();
        mPosition = new Position(mLayoutParams, new Point(0, 0), getMaxTriggerViewPosition(mCtx));

        mScaleUpAnim = new ScaleAnimation(1, ANIM_SIZE, 1, ANIM_SIZE, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mScaleUpAnim.setDuration(ANIM_DURATION);
        mScaleUpAnim.setFillAfter(true);
        mScaleDownAnim = new ScaleAnimation(ANIM_SIZE, 1, ANIM_SIZE, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mScaleDownAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                mTriggerbutton.setBackgroundResource(R.drawable.ic_instalate_trigger_background);    // changing background to ripple drawable
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mScaleDownAnim.setDuration(ANIM_DURATION);
    }

    public void addAtPosition(int x, int y) {
        remove();
        mPosition.setMax(getMaxTriggerViewPosition(mCtx));      // onConfigurationChanged() is NOT called after view is removed from windowmanager, so we need to update min/max values here to reflect possible configuration changes that happened while view was not shown
        mPosition.setX(x);
        mPosition.setY(y);
        mWindowManager.addView(this, mLayoutParams);
        sendNewDropPositionToListeners();       // if configuration changed (see above) send updated position to listeners
    }

    public void addAtPositionRelative(float x, float y) {
        remove();
        mPosition.setMax(getMaxTriggerViewPosition(mCtx));
        mPosition.setXRelative(x);
        mPosition.setYRelative(y);
        mWindowManager.addView(this, mLayoutParams);
        sendNewDropPositionToListeners();
    }

    private WindowManager.LayoutParams newLayoutParams() {
        // int w, int h, int _type, int _flags, int _format
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
                    TYPE_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);
        params.windowAnimations = android.R.style.Animation_Dialog;
        params.gravity = Gravity.START | Gravity.TOP;   // screen origin is always left/top
        return params;
    }

    private Point getMaxTriggerViewPosition(Context c) {
        float dragViewLayoutWH = c.getResources().getDimension(R.dimen.trigger_btn_wh) +
                    2 * c.getResources().getDimension(R.dimen.trigger_btn_margin);
        return new Point((int) (DroidUtils.getScreenWidthMinusInsets(mCtx) - dragViewLayoutWH),
                    (int) (DroidUtils.getScreenHeightMinusInsets(mCtx) - dragViewLayoutWH));
    }

    public void remove() {
        if (isShown()) {
            if (mDragActive) {
                mDragActive = false;
                sendNewDropPositionToListeners();
            }
            mHandler.removeCallbacks(this::remove);    // remove any pending callbacks (if remove() is called before Runnable.run() has ended)
            mWindowManager.removeView(this);
        }
    }

    public void removeDelayed(int seconds) {
        mHandler.postDelayed(this::remove, seconds * 1000);
    }

    public void setPosition(int x, int y) {
        mPosition.setX(x);
        mPosition.setY(y);
        if (isShown()) {
            mWindowManager.updateViewLayout(this, mLayoutParams);
        }
//        invalidate();       // ?is this required?
    }

    public void setPositionRelative(float x, float y) {
        mPosition.setXRelative(x);
        mPosition.setYRelative(y);
        if (isShown()) {
            mWindowManager.updateViewLayout(this, mLayoutParams);
        }
    }


    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mTriggerbutton = findViewById(R.id.trigger_button);     // findViewById() returns null if invoked in constructor
        setListener();
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        // configurations: orientation, screensize, fontsize, iconsize, locale, density, hardware-keyboard...
        super.onConfigurationChanged(newConfig);
        mPosition.setMax(getMaxTriggerViewPosition(mCtx));
        if (isShown()) {
            mWindowManager.updateViewLayout(this, mLayoutParams);
        }
        sendNewDropPositionToListeners();
    }


    @SuppressLint("ClickableViewAccessibility")
    // We can ignore the warning "onTouch should call View.performClick() when a click is detected" here.
    // performClick() is already called because we don't consume MotionEvent.ACTION_DOWN and MotionEvent.UP events
    // We also don't need to override performClick() because super.performClick() is called.
    private void setListener() {
        mTriggerbutton.setOnLongClickListener((View view) -> {
            mDragActive = true;
            setNewPointerPos();
            view.setBackgroundResource(android.R.color.transparent);
            view.startAnimation(mScaleUpAnim);
            return true;
        });


        mTriggerbutton.setOnTouchListener((View view, MotionEvent event) -> {
            // event.getRawXY(): position of cursor in device screen
            mInScreen.x = event.getRawX();
            mInScreen.y = event.getRawY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                // ACTION_DOWN/UP: first/last finger enters/leaves view
                // ACTION_POINTER_DOWN/UP: extra finger enters/leaves view

                case MotionEvent.ACTION_DOWN:
                    // view.getXY(): position of left (upper) border of view to left (upper) border of parent
                    // event.getXY(): position of cursor in view

                    mOffset.x = mInScreen.x - mPosition.getX() - event.getX() + view.getWidth() / 2f;
                    mOffset.y = mInScreen.y - mPosition.getY() - event.getY() + view.getHeight() / 2f;
                    return false;   // don't consume (Click&LongClickListener need this event)

                case MotionEvent.ACTION_UP:
                    if (mDragActive) {
                        mDragActive = false;
                        view.startAnimation(mScaleDownAnim);
                        sendNewDropPositionToListeners();
                    }
                    return false;

                case MotionEvent.ACTION_MOVE:
                    if (mDragActive) {
                        setNewPointerPos();
                        return true;
                    }
                    return false;
            }
            return false;   // true if event should be consumed, false otherwise
        });
    }

    private void setNewPointerPos() {
        setPosition((int) (mInScreen.x - mOffset.x), (int) (mInScreen.y - mOffset.y));
    }


    private void sendNewDropPositionToListeners() {
        if (mOnNewDropPositionListener != null) {
            Point curPos = mPosition.get();
            if (!curPos.equals(mLastDropPos)) {
                mLastDropPos.set(curPos.x, curPos.y);
                mOnNewDropPositionListener.onNewDropPosition(curPos.x, curPos.y);
            }
        }
        if (mOnNewRelativeDropPositionListener != null) {
            PointF curPosRel = mPosition.getRelative();
            if (!curPosRel.equals(mLastRelDropPos)) {
                mLastRelDropPos.set(curPosRel.x, curPosRel.y);
                mOnNewRelativeDropPositionListener.onNewRelativeDropPosition(curPosRel.x, curPosRel.y);
            }
        }
    }


    private void logData(View view, MotionEvent event) {
        Log.d(TAG, "mOffset.x: " + mOffset.x);
        Log.d(TAG, "mOffset.y: " + mOffset.y);
        Log.d(TAG, "event.getRawX(): " + mInScreen.x);
        Log.d(TAG, "event.getRawY(): " + mInScreen.y);
        Log.d(TAG, "mLayoutParams.x: " + mLayoutParams.x);
        Log.d(TAG, "mLayoutParams.y: " + mLayoutParams.y);
        Log.d(TAG, "event.getX(): " + event.getX());
        Log.d(TAG, "event.getY(): " + event.getY());
        MotionEvent.PointerCoords pointerCoords = new MotionEvent.PointerCoords();
        event.getPointerCoords(0, pointerCoords);
        Log.d(TAG, "pointerCoords.x: " + pointerCoords.x);
        Log.d(TAG, "pointerCoords.y: " + pointerCoords.y);
        int[] location = new int[2];
        getLocationOnScreen(location);
        Log.d(TAG, "getLocationOnScreen [0]: " + (location[0]));
        Log.d(TAG, "getLocationOnScreen [1]: " + location[1]);

        Log.d(TAG, "getLocationOnScreen + event.getX(): " + (location[0]
                    + getResources().getDimensionPixelOffset(R.dimen.trigger_btn_margin)
                    + event.getX()));
        Log.d(TAG, "getLocationOnScreen + event.getY(): " + (location[1]
                    + getResources().getDimensionPixelOffset(R.dimen.trigger_btn_margin)
                    + event.getY()));

        Log.d(TAG, "DragViewLayout.this.getX(): " + TriggerView.this.getX());
        Log.d(TAG, "DragViewLayout.this.getX(): " + TriggerView.this.getY());

        Log.d(TAG, "mTriggerbutton.getX(): " + mTriggerbutton.getX());
        Log.d(TAG, "mTriggerbutton.getX(): " + mTriggerbutton.getY());

        Point p = new Point();
        mWindowManager.getDefaultDisplay().getSize(p);
        Log.d(TAG, "mWindowManager.getDefaultDisplay().getSize.x: " + p.x);
        Log.d(TAG, "mWindowManager.getDefaultDisplay().getSize.y: " + p.y);

        Log.d(TAG, "Resources.getSystem().getDisplayMetrics().widthPixels: "
                    + Resources.getSystem().getDisplayMetrics().widthPixels);
        Log.d(TAG, "Resources.getSystem().getDisplayMetrics().heightPixels: "
                    + Resources.getSystem().getDisplayMetrics().heightPixels);

        Log.d(TAG, "my custom fun width: " + DroidUtils.getScreenWidthMinusInsets(mCtx));
        Log.d(TAG, "my custom fun height: " + DroidUtils.getScreenHeightMinusInsets(mCtx));

        Log.d(TAG, "mMaxLayoutParams.x: " + mPosition.getXMax());
        Log.d(TAG, "mMaxLayoutParams.y: " + mPosition.getYMax());

        Log.d(TAG, "--------");
    }


    public interface OnNewDropPositionListener {
        void onNewDropPosition(int newX, int newY);
    }

    public interface OnNewRelativeDropPositionListener {
        void onNewRelativeDropPosition(float newX, float newY);
    }

    public void setOnNewDropPositionListener(OnNewDropPositionListener listener) {
        mOnNewDropPositionListener = listener;
    }
    public void setOnNewRelativeDropPositionListener(OnNewRelativeDropPositionListener listener) {
        mOnNewRelativeDropPositionListener = listener;
    }


    // wrapper class to manage relative positioning and clamping of LayoutParams to available screen-space
    private static class Position {
        private final Point mMin;
        private final Point mMax;
        private final Point mPos = new Point();
        private final PointF mPosRel = new PointF();
        private final WindowManager.LayoutParams mParams;

        Position(WindowManager.LayoutParams params, Point min, Point max) {
            mParams = params;
            mMin = min;
            mMax = max;
            setX(params.x);
            setY(params.y);     // clamp params.x/y and set mPos & mPosRel
        }


        public Point get() {
            setX(mParams.x);
            setY(mParams.y);    // params could have been modified outside of Position object, so clamp and update mPos and mPosRel
            return mPos;
        }
        public int getX() {
            setX(mParams.x);
            return mPos.x;
        }
        public int getY() {
            setY(mParams.y);
            return mPos.y;
        }
        public void set(Point p) {
            setX(p.x);
            setY(p.y);
        }
        public void setX(int x) {
            mPos.x = clamp(mMin.x, x, mMax.x);
            updateXParam();
            updateXRelative();
        }
        public void setY(int y) {
            mPos.y = clamp(mMin.y, y, mMax.y);
            updateYParam();
            updateYRelative();
        }


        public PointF getRelative() {
            return mPosRel;
        }
        public float getXRelative() {
            return mPosRel.x;
        }
        public float getYRelative() {
            return mPosRel.y;
        }
        public void setXRelative(float x) {
            mPosRel.x = clamp(0f, x, 1f);
            updateXAbsolute();
        }
        public void setYRelative(float y) {
            mPosRel.y = clamp(0f, y, 1f);
            updateYAbsolute();
        }


        public void setMin(Point min) {
            setXMin(min.x);
            setYMin(min.y);
        }
        public void setXMin(int min) {
            mMin.x = min;
            updateXAbsolute();
        }
        public void setYMin(int min) {
            mMin.y = min;
            updateYAbsolute();
        }

        public void setMax(Point max) {
            setXMax(max.x);
            setYMax(max.y);
        }
        public void setXMax(int max) {
            mMax.x = max;
            updateXAbsolute();
        }
        public void setYMax(int max) {
            mMax.y = max;
            updateYAbsolute();
        }

        public Point getMin() {
            return mMin;
        }
        public int getXMin() {
            return mMin.x;
        }
        public int getYMin() {
            return mMin.y;
        }
        public Point getMax() {
            return mMax;
        }
        public int getXMax() {
            return mMax.x;
        }
        public int getYMax() {
            return mMax.y;
        }


        private int clamp(int min, int val, int max) {
            return Math.max(min, Math.min(max, val));
        }
        private float clamp(float min, float val, float max) {
            return Math.max(min, Math.min(max, val));
        }


        private void updateXAbsolute() {
            mPos.x = (int) (mMin.x + mPosRel.x * (mMax.x - mMin.x));
            updateXParam();
        }
        private void updateYAbsolute() {
            mPos.y = (int) (mMin.y + mPosRel.y * (mMax.y - mMin.y));
            updateYParam();
        }
        private void updateXRelative() {
            mPosRel.x = (float) (mPos.x - mMin.x) / (mMax.x - mMin.x);
        }
        private void updateYRelative() {
            mPosRel.y = (float) (mPos.y - mMin.y) / (mMax.y - mMin.y);
        }
        private void updateXParam() {
            mParams.x = mPos.x;
        }
        private void updateYParam() {
            mParams.y = mPos.y;
        }
    }
}
