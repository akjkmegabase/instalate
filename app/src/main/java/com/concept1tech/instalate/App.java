/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.view.Gravity;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceManager;

import com.concept1tech.unn.DroidUtils;

public class App extends Application {

    public static final String TRANSL_SERV_NOTICHAN_ID = "service_noti_chan";

    public static final String TRANSLATION_DATA_EXTRA = BuildConfig.APPLICATION_ID + ".TRANSLATION_DATA_EXTRA";

    public static final int TRANSL_NOTI_ID = 1;     // required should we need to update foreground service
    public static final int OVERLAY_PERM_RESULT = 1;


    @Override
    public void onCreate() {
        super.onCreate();
        setUpServiceNotiChan();

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {    // there's no duration pref so show button always
            DroidUtils.prefsPutString(this, "prefkey_trigger_duration", "always");
        }
    }

    // noti channels are mandatory on OREO and higher
    private void setUpServiceNotiChan() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel serviceChan = new NotificationChannel(TRANSL_SERV_NOTICHAN_ID,
                        getString(R.string.transl_serv_notichan_name),
                        NotificationManager.IMPORTANCE_LOW);  // needs to be at least LOW for foreground service
            serviceChan.setShowBadge(false);
//            // increase importance to keep notification as far at top as possible (is ongoing noti always on top?)
//            serviceChan.setImportance(NotificationManager.IMPORTANCE_HIGH);
//            serviceChan.enableLights(false);
//            serviceChan.enableVibration(false);
//            serviceChan.setLockscreenVisibility(Notification.VISIBILITY_SECRET);
            NotificationManager notiManager = getSystemService(NotificationManager.class);
            notiManager.createNotificationChannel(serviceChan);
        }
    }


    // only called in emulated process environments!
    // (we shouldn't need to unregister receiver anyways though
    // see: https://stackoverflow.com/a/49369755)
    @Override
    public void onTerminate() {
        super.onTerminate();
    }


    static void toast(Context ctx, String msg) {    // for toasts pass app ctx preferably
        Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    // get shared pref methods
    public static String getCurProviderFromSPrefs(Context ctx) {
        SharedPreferences defaultSPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        return defaultSPref.getString("prefkey_provider", "dictcc");
    }
    public static void putActiveLangsToSPrefs(Context ctx, String[] activeProps) {
        SharedPreferences defaultSPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor prefEditor = defaultSPref.edit();
        prefEditor.putString("active_language_1", activeProps[0]);
        prefEditor.putString("active_direction", activeProps[1]);
        prefEditor.putString("active_language_2", activeProps[2]);
        prefEditor.apply();     // or prefEditor.commit(); to apply immediately
    }
    public static String[] getActiveLangsFromSPrefs(Context ctx) {
        SharedPreferences defaultSPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        String[] activeProps = new String[3];
        activeProps[0] = defaultSPref.getString("active_language_1", "0");
        activeProps[1] = defaultSPref.getString("active_direction", "0");
        activeProps[2] = defaultSPref.getString("active_language_2", "1");
        return activeProps;
    }
}
