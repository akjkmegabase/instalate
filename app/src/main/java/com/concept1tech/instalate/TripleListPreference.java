/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;

import java.util.List;

public class TripleListPreference extends Preference {

    private Context mCtx;

    @SuppressWarnings("unchecked")
    private List<EntryItem>[] mEntryItems = new List[3];
    private String[] mActiveValues = new String[3];
    private OnPreferenceChangeListener mOnPrefChangeListener;
    private final OnPreferenceButtonChangeListener[] mOnPrefButtonChangeListeners = new OnPreferenceButtonChangeListener[3];

    private String[] mTitles = {"Title", "Title", "Title"};
    private final String mNegativeButtonText = "Cancel";


    public TripleListPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public TripleListPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public TripleListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TripleListPreference(Context context) {
        super(context);
        init(context);
    }


    private void init(Context context) {
        mCtx = context;
        setLayoutResource(R.layout.preference_triple_list);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);

        // TODO: re-use views?
        holder.itemView.setClickable(false);
        // Views returned by holder.findViewbyId() are actually cached (not as costly as activity's findViewbyID())
        // Can't hold refs to views because they are recycled! (see docs)
        final ImageView[] btns = new ImageView[3];
        btns[0] = (ImageView) holder.findViewById(R.id.imageButton1);
        btns[1] = (ImageView) holder.findViewById(R.id.imageButton2);
        btns[2] = (ImageView) holder.findViewById(R.id.imageButton3);

        for (int i = 0; i < 3; i++) {
            final int btnIdx = i;
            btns[btnIdx].setImageResource(findIconOfValue(btnIdx, mActiveValues));
            btns[btnIdx].setOnClickListener((View view) -> {
                int activeItemIdx = findIndexOfValue(btnIdx, mActiveValues);
                final MyListAdapter adapter = new MyListAdapter(getContext(), mEntryItems[btnIdx], activeItemIdx);

                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                            .setTitle(mTitles[btnIdx])
//                                    .setIcon(mDialogIcon)
                            .setPositiveButton(null, null)
                            .setNegativeButton(mNegativeButtonText, null)
                            .setSingleChoiceItems(adapter, activeItemIdx, (DialogInterface dialog, int which) -> {
                                mActiveValues[btnIdx] = mEntryItems[btnIdx].get(which).getValue();
                                btns[btnIdx].setImageResource(findIconOfValue(btnIdx, mActiveValues));

                                // send to listeners
                                if (mOnPrefChangeListener != null) {
                                    mOnPrefChangeListener.onPreferenceChange(
                                                TripleListPreference.this,
                                                mActiveValues);
                                }
                                if (mOnPrefButtonChangeListeners[btnIdx] != null) {
                                    mOnPrefButtonChangeListeners[btnIdx].onPreferenceButtonChange(
                                                TripleListPreference.this,
                                                btnIdx,
                                                mActiveValues[btnIdx]);
                                }
                                App.putActiveLangsToSPrefs(mCtx.getApplicationContext(), mActiveValues);
                                dialog.dismiss();
                            });
                builder.create().show();
            });
        }
    }

    public void setEntryItems(List<EntryItem>[] items) {
        mEntryItems = items;
    }

    public void setActiveValues(String[] activeValues) {
        mActiveValues = activeValues;
        App.putActiveLangsToSPrefs(mCtx.getApplicationContext(), activeValues);
        notifyChanged();
    }

    public void setOnPreferenceChangeListener(OnPreferenceChangeListener listener) {
        mOnPrefChangeListener = listener;
    }
    public void setOnPreferenceButtonChangeListener(OnPreferenceButtonChangeListener listener, int btnIdx) {
        mOnPrefButtonChangeListeners[btnIdx] = listener;
    }

    public void setTitles(String[] titles) {
        mTitles = titles;
    }

    public Integer findIndexOfValue(int btnIdx, String[] values) {
        List<EntryItem> entryItems = mEntryItems[btnIdx];
        String value = values[btnIdx];
        if (value != null && entryItems != null) {
            for (int i = entryItems.size() - 1; i >= 0; i--) {
                if (entryItems.get(i).getValue().equals(value)) {
                    return i;
                }
            }
        }
        return null;
    }

    public Integer findIconOfValue(int btnIdx, String[] values) {
        Integer idx = findIndexOfValue(btnIdx, values);
        return idx != null ? mEntryItems[btnIdx].get(idx).getIcon() : 0;
    }


    private static class MyListAdapter extends BaseAdapter {
        final Context mCtx;
        final List<EntryItem> mEntryItems;
        final int mCurIdx;

        MyListAdapter(Context ctx, List<EntryItem> entryItems, int curActive) {
            mCtx = ctx;
            mEntryItems = entryItems;
            mCurIdx = curActive;
        }

        @Override
        public int getCount() {
            return mEntryItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mEntryItems.get(position);  // necessary?
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View listItem, @NonNull ViewGroup parent) {
            if (listItem == null) {   // check if view is new or reused
                listItem = LayoutInflater.from(mCtx).inflate(R.layout.my_select_dialog_singlechoice_material, parent, false);
            }
            // if performance is important consider ViewHolder pattern which reduces calls to findViewById()
            CheckedTextView tv = listItem.findViewById(R.id.checked_text_android);
            ImageView iv = listItem.findViewById(R.id.my_image_view);

            EntryItem curEntry = mEntryItems.get(position);
            tv.setText(curEntry.getEntry());
            tv.setChecked(position == mCurIdx);
            iv.setImageResource(curEntry.isIconVisible() ? curEntry.getIcon() : 0);

            return listItem;
        }
    }

    public interface OnPreferenceChangeListener {
        boolean onPreferenceChange(Preference preference, Object[] allValues);
    }

    public interface OnPreferenceButtonChangeListener {
        boolean onPreferenceButtonChange(Preference preference, int btnIdx, Object newValue);
    }
}
