/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.concept1tech.unn.ParcelableCharSequence;

import java.util.List;

public class TranslationAdapter extends RecyclerView.Adapter<TranslationAdapter.ViewHolder> {

    private List<ParcelableCharSequence> mTranslationItems;
    private final LayoutInflater mInflater;
    private final float mFontSize;

    public TranslationAdapter(Context ctx, float fs) {
        mInflater = LayoutInflater.from(ctx);
        mFontSize = fs;
    }

    public void setTranslationItems(List<ParcelableCharSequence> translationItems) {
        mTranslationItems = translationItems;
    }

    @NonNull
    @Override
    public TranslationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_translation, parent, false);
        return new ViewHolder(itemView, mFontSize);
    }
    @Override
    public void onBindViewHolder(@NonNull TranslationAdapter.ViewHolder viewHolder, int position) {
        if (mTranslationItems != null) {
            viewHolder.setText(mTranslationItems.get(position).get());
        }
    }
    @Override
    public int getItemCount() {
        return mTranslationItems == null ? 0 : mTranslationItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mItemTv;

        public ViewHolder(@NonNull View itemView, float fs) {
            super(itemView);
            mItemTv = itemView.findViewById(R.id.tv_translation_item);
            mItemTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, fs);
        }

        public void setText(CharSequence cs) {
            mItemTv.setText(cs);
        }
    }
}
