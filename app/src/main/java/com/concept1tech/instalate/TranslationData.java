/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.concept1tech.unn.DroidUtils;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import java.util.ArrayList;
import java.util.List;

public class TranslationData implements Parcelable {

    private PageRequest mRequest = new PageRequest();
    private PageResponse mResponse = new PageResponse();
    private String mProviderName = "";
    private int mProviderIcon;
    private Uri mBrowserLink;
    private String mOrigClip = "";
    private List<ParcelableCharSequence> mTList = new ArrayList<>();

    public TranslationData(String origClip) {
        mOrigClip = origClip;
    }

    public PageRequest getRequest() {
        return mRequest;
    }
    public void setRequest(PageRequest request) {
        mRequest = request;
    }
    public PageResponse getResponse() {
        return mResponse;
    }
    public void setResponse(PageResponse response) {
        mResponse = response;
    }

    public String getProviderName() {
        return mProviderName;
    }
    public void setProviderName(String name) {
        mProviderName = name;
    }
    public int getProviderIcon() {
        return mProviderIcon;
    }
    public void setProviderIcon(int iconResource) {
        mProviderIcon = iconResource;
    }
    /**
     * @return URI to be viewed in the browser (defaults to search request URI if not set by {@link #setBrowserLink(Uri)})
     */
    public Uri getBrowserLink() {
        return mBrowserLink != null ? mBrowserLink : mRequest.getUri();
    }
    /**
     * Set this to view this URI in the browser instead of the search request URI.
     *
     * @param link URI to be viewed in the browser
     */
    public void setBrowserLink(Uri link) {
        mBrowserLink = link;
    }
    public String getOrigClip() {
        return mOrigClip;
    }
    public void setOrigClip(String origClip) {
        mOrigClip = origClip;
    }
    public List<ParcelableCharSequence> getTranslationList() {
        return mTList;
    }
    public void setTranslationList(List<ParcelableCharSequence> tList) {
        mTList = tList;
    }


    protected TranslationData(Parcel in) {
        mRequest = in.readParcelable(PageRequest.class.getClassLoader());
        mResponse = in.readParcelable(PageResponse.class.getClassLoader());
        mProviderName = in.readString();
        mProviderIcon = in.readInt();
        mBrowserLink = in.readParcelable(Uri.class.getClassLoader());
        mOrigClip = in.readString();
        mTList = DroidUtils.readListFromParcel(in, mTList, ParcelableCharSequence.class);
    }
    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeParcelable(mRequest, flags);
        parcel.writeParcelable(mResponse, flags);
        parcel.writeString(mProviderName);
        parcel.writeInt(mProviderIcon);
        parcel.writeParcelable(mBrowserLink, flags);
        parcel.writeString(mOrigClip);
        DroidUtils.writeArrayListToParcel(parcel, mTList);
    }
    public static final Creator<TranslationData> CREATOR = new Creator<TranslationData>() {
        @Override
        public TranslationData createFromParcel(Parcel in) {
            return new TranslationData(in);
        }
        @Override
        public TranslationData[] newArray(int size) {
            return new TranslationData[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }
}
