/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.preference.PreferenceManager;

import com.concept1tech.unn.DroidUtils;

import static com.concept1tech.instalate.App.TRANSL_NOTI_ID;
import static com.concept1tech.instalate.App.TRANSL_SERV_NOTICHAN_ID;


public class ClipboardListenerService extends Service implements ClipboardManager.OnPrimaryClipChangedListener {

    public static final String ACTION_START = BuildConfig.APPLICATION_ID + ".ACTION_START";
    public static final String ACTION_RESTART = BuildConfig.APPLICATION_ID + ".ACTION_RESTART";
    public static final String ACTION_SHOW_BUTTON = BuildConfig.APPLICATION_ID + ".ACTION_SHOW_BUTTON";
    public static final String ACTION_REMOVE_BUTTON = BuildConfig.APPLICATION_ID + ".ACTION_REMOVE_BUTTON";
    public static final String ACTION_REPOSITION_BUTTON = BuildConfig.APPLICATION_ID + ".ACTION_REPOSITION_BUTTON";

    public static boolean sIsStarted = false;

    private ClipboardManager mClipboardManager;
    private SharedPreferences mDefaultSPref;
    private TriggerView mTriggerView;

    @SuppressLint("InflateParams")
    @Override
    public void onCreate() {
        super.onCreate();

        mClipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        if (mClipboardManager != null) {
            mClipboardManager.addPrimaryClipChangedListener(this);
        }

        mDefaultSPref = PreferenceManager.getDefaultSharedPreferences(this);

        mTriggerView = (TriggerView) LayoutInflater.from(this).inflate(R.layout.view_trigger, null);
        (mTriggerView.findViewById(R.id.trigger_button)).setOnClickListener((View view) -> {
            startActivity(TranslationDialogActivity.getBaseIntent(getApplicationContext()));
            if (!triggerButtonShowsAlways()) {
                mTriggerView.remove();
            }
        });
        mTriggerView.setOnNewRelativeDropPositionListener((float newX, float newY) ->
                    DroidUtils.prefsPutString(ClipboardListenerService.this, "prefkey_trigger_position", newX + "," + newY));
    }

    // not called if containing process is killed
    @Override
    public void onDestroy() {
        super.onDestroy();
        mClipboardManager.removePrimaryClipChangedListener(this);   // not calling this makes the service probably not getting garbage collected
        DroidUtils.prefsPutBoolean(getApplicationContext(), "prefkey_service_active", false);
        sIsStarted = false;
        if (mTriggerView != null) mTriggerView.remove();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent == null ? ACTION_RESTART : intent.getAction();
        switch (action == null ? "" : action) {
            case ACTION_START:
            case ACTION_RESTART:    // need to call startForeground() again (otherwise crashes e.g. when removing settings activity from recents)
                // Make service foreground so it now has the same priority as an activity (see https://developer.android.com/guide/components/activities/process-lifecycle)
                // Dont need to call NotificationManager.notify() here because service does it itself
                startForeground(TRANSL_NOTI_ID, getSettingsNoti());
                sIsStarted = true;
                DroidUtils.prefsPutBoolean(getApplicationContext(), "prefkey_service_active", true);
                if (triggerButtonShowsAlways()) {
                    showTriggerView();
                }
                break;
            case ACTION_SHOW_BUTTON:
                if (sIsStarted) {
                    showTriggerView();
                }
                break;
            case ACTION_REMOVE_BUTTON:
                if (sIsStarted && mTriggerView != null) {
                    mTriggerView.remove();
                }
                break;
            case ACTION_REPOSITION_BUTTON:
                if (sIsStarted) {
                    updateTriggerViewPosition();
                }
                break;
            default:
                break;
        }
        return START_STICKY;    // START_REDELIVER_INTENT re-delivers last intent which could be ACTION_SHOW_BUTTON, which would be wrong
    }


    @Override
    public void onPrimaryClipChanged() {
        if (!triggerButtonShowsAlways()) {
            showTriggerView();
        }
    }


    private void showTriggerView() {
        if (!DroidUtils.hasOverlayPermission(this)) {
            App.toast(getApplicationContext(), getString(R.string.overlay_permission));
            return;
        }

        if (mTriggerView != null) {
            String position = mDefaultSPref.getString("prefkey_trigger_position", getString(R.string.trigger_position_default_value));
            String[] parts = position.split(",");
            mTriggerView.addAtPositionRelative(Float.parseFloat(parts[0]), Float.parseFloat(parts[1]));

            if (!triggerButtonShowsAlways()) {
                String duration = mDefaultSPref.getString("prefkey_trigger_duration", getString(R.string.trigger_duration_default_value));
                mTriggerView.removeDelayed(Integer.parseInt(duration));
            }
        }
    }

    private void updateTriggerViewPosition() {
        if (mTriggerView != null) {
            String position = mDefaultSPref.getString("prefkey_trigger_position", getString(R.string.trigger_position_default_value));
            String[] parts = position.split(",");
            mTriggerView.setPositionRelative(Float.parseFloat(parts[0]), Float.parseFloat(parts[1]));
        }
    }

    private Notification getSettingsNoti() {
        Intent clickIntt = new Intent(this, PrefsActivity.class);
        NotificationCompat.Builder builder = getBaseNoti(clickIntt)
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .setContentText(getString(R.string.show_settings));
        return builder.build();
    }

    private Notification getTranslationNoti() {
        Intent clickIntt = TranslationDialogActivity.getBaseIntent(getApplicationContext());
        Intent buttonIntt = new Intent(this, PrefsActivity.class);
        PendingIntent buttonPIntent = PendingIntent.getActivity(this, 1, buttonIntt,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = getBaseNoti(clickIntt)
                    .addAction(R.drawable.ic_settings, getString(R.string.settings), buttonPIntent)  //  action icons only show for API<24, on wearables or media style notifications
                    .setContentText(getString(R.string.transl_clipboard))
//                                        .setColorized(true)
//                                        .setColor(getColor(R.color.colorPrimary))
                    ;
        return builder.build();
    }

    private NotificationCompat.Builder getBaseNoti(Intent clickIntt) {
//        clickIntt.setPackage(BuildConfig.APPLICATION_ID);   // restrict sending broadcasts only to components with our package name
        PendingIntent clickPIntt = PendingIntent.getActivity(this, 0, clickIntt,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        return new NotificationCompat.Builder(this, TRANSL_SERV_NOTICHAN_ID)      // wont crash on pre ORIO APIs (no noti channels)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentIntent(clickPIntt)
                    .setSmallIcon(R.drawable.ic_transl_is_act)     // for API<21 icon has to be white always to show up
                    // .setOngoing(true)   // if noti set by foreground, ongoing (un-deleteable/un-swipeable) is default
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.collapse_banner));  // large icon (icon in the notification drawer) doesn't draw correctly on API<21 (?) and is required to be .png on API>=21 (?)
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    static String getTextItemInClipboard(ClipboardManager clipboardManager) {
        ClipData clipData = clipboardManager.getPrimaryClip();
        if (clipData != null && clipData.getItemCount() > 0) {
            ClipDescription clipDesc = clipData.getDescription();
            if (clipDesc != null) {
                String curType = clipDesc.getMimeType(0);
                if (ClipDescription.compareMimeTypes(
                            ClipDescription.MIMETYPE_TEXT_PLAIN, curType) ||
                            ClipDescription.compareMimeTypes(
                                        ClipDescription.MIMETYPE_TEXT_HTML, curType)) {
                    return clipData.getItemAt(0).getText().toString();
                }
            }
        }
        return null;
    }

    private boolean triggerButtonShowsAlways() {
        String duration = mDefaultSPref.getString("prefkey_trigger_duration", getString(R.string.trigger_duration_default_value));
        return duration.equals("always");
    }
}
