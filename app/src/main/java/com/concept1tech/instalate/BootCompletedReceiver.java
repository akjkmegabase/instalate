/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String intentAction = intent.getAction();
        if (intentAction != null && intentAction.equals(Intent.ACTION_BOOT_COMPLETED)) {
            SharedPreferences defaultSPref = PreferenceManager.getDefaultSharedPreferences(context);
            if (defaultSPref.getBoolean("prefkey_service_active", false) && defaultSPref.getBoolean("prefkey_on_boot", false)) {
                Intent serviceIntt = new Intent(context, ClipboardListenerService.class);
                serviceIntt.setAction(ClipboardListenerService.ACTION_START);
                ContextCompat.startForegroundService(context, serviceIntt);
            }
        }
    }
}
