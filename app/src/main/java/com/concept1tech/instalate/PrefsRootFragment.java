/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.SwitchPreferenceCompat;

import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.DroidUtils;
import com.concept1tech.unn.NetworkUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PrefsRootFragment extends PreferenceFragmentCompat {

    public static final String TAG = PrefsRootFragment.class.getSimpleName();

    private Context mAppCtx;
    private SwitchPreferenceCompat mPrefServActive;
    private TripleListPreference mLanguagesPref;
    private ListPreference mPrefPosition;
    private SharedPreferences mDefaultSPref;
    private SharedPreferences.OnSharedPreferenceChangeListener mSPrefListener;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mAppCtx = context.getApplicationContext();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // ClipboardListenerService.sIsStarted detects if the service is inactive even if it was killed by the system before (e.g. on device shutdown) in which case onDestroy() of service is NOT called. This works because sIsStarted is set to false as early as the classloader loads ClipboardListenerService
        // needs to be called before setPreferencesFromResource() which will set the switch preference
        DroidUtils.prefsPutBoolean(mAppCtx, "prefkey_service_active", ClipboardListenerService.sIsStarted);
        setPreferencesFromResource(R.xml.prefs_root, rootKey);

        mPrefServActive = findPreference("prefkey_service_active");
        ListPreference providerPref = findPreference("prefkey_provider");
        mLanguagesPref = findPreference("prefkey_languages");
        SwitchPreferenceCompat prefShowInMenu = findPreference("prefkey_show_in_context_menu");
        mPrefPosition = findPreference("prefkey_trigger_position");
        Preference prefSource = findPreference("prefkey_source_code");

        mLanguagesPref.setTitles(new String[]{getString(R.string.choose_language), getString(R.string.choose_direction), getString(R.string.choose_language)});
        setActiveLanguages(App.getCurProviderFromSPrefs(mAppCtx));

        prefShowInMenu.setChecked(isContextMenuEnabled());     // sync app (and shared pref) to component setting (can be out of sync for example if app data has been cleared)

        // listeners
        mPrefServActive.setOnPreferenceChangeListener((Preference preference, Object newValue) -> {
            Boolean enabled = (Boolean) newValue;
            if (enabled) {
                if (!DroidUtils.hasOverlayPermission(mAppCtx)) {
                    requestOverlayPermission();
                } else {
                    startClipboardService();
                }
            } else {
                stopClipboardService();
            }
            return true;
        });
        providerPref.setOnPreferenceChangeListener((Preference preference, Object newValue) -> {
            String newProv = (String) newValue;
            setActiveLanguages(newProv);
            return true;
        });
        prefShowInMenu.setOnPreferenceChangeListener((Preference preference, Object newValue) -> {
            setContextMenuEnabled((boolean) newValue);
            return true;
        });
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            ListPreference prefDuration = findPreference("prefkey_trigger_duration");
            prefDuration.setOnPreferenceChangeListener((Preference preference, Object newValue) -> {
                String newDur = (String) newValue;
                if (newDur.equals("always")) {
                    updateClipboardService(ClipboardListenerService.ACTION_SHOW_BUTTON);
                } else {
                    updateClipboardService(ClipboardListenerService.ACTION_REMOVE_BUTTON);
                }
                return true;
            });
        }
        prefSource.setOnPreferenceClickListener((Preference preference) -> {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                NetworkUtils.view(activity, Uri.parse(getString(R.string.project_home)));
            }
            return true;
        });

        mLanguagesPref.setOnPreferenceChangeListener((Preference preference, Object[] allValues) -> {
            if (allValues != null) {
                String[] allValuesS = (String[]) allValues;
                setLanguages(App.getCurProviderFromSPrefs(mAppCtx), allValuesS);
            }
            return true;
        });


        // floating button
        // if visible right now update position
        mPrefPosition.setOnPreferenceChangeListener((Preference preference, Object newValue) -> {
            updateClipboardService(ClipboardListenerService.ACTION_REPOSITION_BUTTON);
            return true;
        });
        // set to "Custom" if position changes happened while this activity did not exist
        final String[] positionEntries = getResources().getStringArray(R.array.trigger_position);
        final String[] positionEntryVals = getResources().getStringArray(R.array.trigger_position_values);
        mDefaultSPref = PreferenceManager.getDefaultSharedPreferences(mAppCtx);
        String triggerPos = mDefaultSPref.getString("prefkey_trigger_position", getString(R.string.trigger_position_default_value));
        if (triggerPos != null && !ArrayUtils.contains(positionEntryVals, triggerPos)) {
            mPrefPosition.setEntries(ArrayUtils.prepend(positionEntries, getString(R.string.custom)));
            mPrefPosition.setEntryValues(ArrayUtils.prepend(positionEntryVals, triggerPos));
            mPrefPosition.setValue(triggerPos);
        }
        // track position changes live while this activity exists
        mSPrefListener = (SharedPreferences sPrefs, String key) -> {
            if (key.equals("prefkey_trigger_position")) {
                String newVal = sPrefs.getString(key, getString(R.string.trigger_position_default_value));
                if (newVal != null) {
                    if (!ArrayUtils.contains(positionEntryVals, newVal)) {
                        mPrefPosition.setEntries(ArrayUtils.prepend(positionEntries, getString(R.string.custom)));
                        mPrefPosition.setEntryValues(ArrayUtils.prepend(positionEntryVals, newVal));
                    } else {
                        mPrefPosition.setEntries(positionEntries);
                        mPrefPosition.setEntryValues(positionEntryVals);
                    }
                    mPrefPosition.setValue(newVal);
                }
            }
        };
        mDefaultSPref.registerOnSharedPreferenceChangeListener(mSPrefListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDefaultSPref != null && mSPrefListener != null) {
            mDefaultSPref.unregisterOnSharedPreferenceChangeListener(mSPrefListener);
        }
    }

    private void setActiveLanguages(String provider) {
        String[] actProps = App.getActiveLangsFromSPrefs(mAppCtx);
        setLanguages(provider, actProps);
    }

    void setLanguages(String providerKey, String[] props) {
        int[] propsI = new int[]{Integer.parseInt(props[0]),
                    Integer.parseInt(props[1]),
                    Integer.parseInt(props[2])};
        Boolean[][][] combinations = Provider.getAll(mAppCtx).get(providerKey).getCombinations();
        validateActiveProps(combinations, propsI);
        int idxActiveLang1 = propsI[0];
        int idxActiveDirect = propsI[1];
        int idxActiveLang2 = propsI[2];

        ArrayList<EntryItem> allLangs = EntryItem.getAll(mAppCtx, R.array.dicts_languages, R.array.dicts_languages_values, R.array.dict_languages_flags);
        ArrayList<EntryItem> allDirects = EntryItem.getAll(mAppCtx, R.array.dicts_directions, R.array.dicts_directions_values, R.array.dicts_directions_icons);
//        for (EntryItem dir : allDirects) dir.setIconVisible(false);

        // get language combinations for current active language (false = not a combination)
        Boolean[] withFalses1 = ArrayUtils.get1dArrayFrom3d(Boolean.class, combinations, 1, idxActiveDirect, idxActiveLang2);
        Boolean[] withFalses2 = ArrayUtils.get1dArrayFrom3d(Boolean.class, combinations, 0, idxActiveLang1, idxActiveLang2);
        Boolean[] withFalses3 = ArrayUtils.get1dArrayFrom3d(Boolean.class, combinations, 2, idxActiveDirect, idxActiveLang1);

        // if EntryItems (languages from resources) or language-combinations vector differ in length cap them
        int minLenLangs = Math.min(withFalses1.length, Math.min(withFalses3.length, allLangs.size()));
        withFalses1 = Arrays.copyOfRange(withFalses1, 0, minLenLangs);
        withFalses3 = Arrays.copyOfRange(withFalses3, 0, minLenLangs);
        List<EntryItem> allLangsCapped = allLangs.subList(0, minLenLangs);

        // remove EntryItems (languages) where, at the same index, the language-combinations vector evaluates to false, sort them afterwards
        @SuppressWarnings("unchecked")
        LinkedList<EntryItem>[] langsArr = new LinkedList[3];
        langsArr[0] = ArrayUtils.removeIfValueInReference(allLangsCapped, withFalses1, false);
        Collections.sort(langsArr[0]);
        langsArr[1] = ArrayUtils.removeIfValueInReference(allDirects, withFalses2, false);
        langsArr[2] = ArrayUtils.removeIfValueInReference(allLangsCapped, withFalses3, false);
        Collections.sort(langsArr[2]);
        mLanguagesPref.setEntryItems(langsArr);

        String[] newPropsS = new String[]{String.valueOf(propsI[0]),
                    String.valueOf(propsI[1]),
                    String.valueOf(propsI[2])};
        mLanguagesPref.setActiveValues(newPropsS);
    }

    private void validateActiveProps(Boolean[][][] comb, int[] props) {
        int lang1 = props[0];
        int direc = props[1];
        int lang2 = props[2];
        int lenLang1 = comb[0].length;
        int lenDirec = comb.length;
        int lenLang2 = comb[0][0].length;
        boolean isOkLang1 = lang1 < lenLang1 && lang1 >= 0; // index out of bounds?
        boolean isOkDirec = direc < lenDirec && direc >= 0;
        boolean isOkLang2 = lang2 < lenLang2 && lang2 >= 0;

        if (!(isOkLang1 && isOkDirec && isOkLang2 && comb[direc][lang1][lang2])) {
            // try to change only direction, language 2 or language 1 (in that order) if combination not found or indices out of bounds
            //
            // vary by direction, return if combination found
            if (isOkLang1 && isOkLang2) {
                Boolean[] dirs = ArrayUtils.get1dArrayFrom3d(Boolean.class, comb, 0, lang1, lang2);
                Integer idx = ArrayUtils.find(dirs);
                if (idx != null) {
                    props[1] = idx;
                    notifyCombinationChange(getString(R.string.provider_doesnt_support_direction));
                    return;
                }
            }
            // vary by language 2, return if combination found
            if (isOkLang1 && isOkDirec) {
                Boolean[] lang2s = ArrayUtils.get1dArrayFrom3d(Boolean.class, comb, 2, direc, lang1);
                Integer idx = ArrayUtils.find(lang2s);
                if (idx != null) {
                    props[2] = idx;
                    notifyCombinationChange(getString(R.string.provider_doesnt_support_language));
                    return;
                }
            }
            // vary by language 1, return if combination found
            if (isOkLang2 && isOkDirec) {
                Boolean[] lang1s = ArrayUtils.get1dArrayFrom3d(Boolean.class, comb, 1, direc, lang2);
                Integer idx = ArrayUtils.find(lang1s);
                if (idx != null) {
                    props[0] = idx;
                    notifyCombinationChange(getString(R.string.provider_doesnt_support_language));
                    return;
                }
            }


            // try to change only direction+language 2 or direction+language 1 (in that order)
            //
            // vary by direction and language 2, return if combination found
            if (isOkLang1) {
                Boolean[][] dirsAndLang2s = ArrayUtils.get2dArrayFrom3d(Boolean[].class, comb, 0, 2, lang1);
                Integer[] idxs = ArrayUtils.find(dirsAndLang2s);
                if (idxs != null) {
                    props[1] = idxs[0];
                    props[2] = idxs[1];
                    notifyCombinationChange(getString(R.string.provider_doesnt_support_language));
                    return;
                }
            }
            // vary by direction and language 1, return if combination found
            if (isOkLang2) {
                Boolean[][] dirsAndLang1s = ArrayUtils.get2dArrayFrom3d(Boolean[].class, comb, 0, 1, lang2);
                Integer[] idxs = ArrayUtils.find(dirsAndLang1s);
                if (idxs != null) {
                    props[1] = idxs[0];
                    props[0] = idxs[1];
                    notifyCombinationChange(getString(R.string.provider_doesnt_support_language));
                    return;
                }
            }


            // fallback to find next valid combination
            Integer[] newProps = ArrayUtils.find(comb);
            props[0] = newProps[1];
            props[1] = newProps[0];
            props[2] = newProps[2];
            notifyCombinationChange(getString(R.string.provider_doesnt_support_language_combination));
        }
    }

    void notifyCombinationChange(final String msg) {
        Log.d(TAG, msg);
        Toast t = Toast.makeText(mAppCtx, msg, Toast.LENGTH_LONG);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
    }


    // "... onResume/Pause() will be called only when the Activities onResume/Pause() are called. They are tightly coupled to the Activity..."
    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
    }


    private void requestOverlayPermission() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage(R.string.overlay_permission)
                        .setPositiveButton(R.string.go_to_settings, (DialogInterface dialog, int id) -> {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                // data of ACTION_MANAGE_OVERLAY_PERMISSION is omitted since R/11/API 30, brings user to top-level screen instead
                                final Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + activity.getPackageName()));
                                try {
                                    PrefsRootFragment.this.startActivityForResult(intent, App.OVERLAY_PERM_RESULT);
                                } catch (ActivityNotFoundException e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                        .setNegativeButton(R.string.cancel,
                                    (DialogInterface dialogInterface, int i) -> mPrefServActive.setChecked(false))
                        .setOnCancelListener((DialogInterface dialogInterface) -> mPrefServActive.setChecked(false));
            builder.create().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == App.OVERLAY_PERM_RESULT) {
            if (DroidUtils.hasOverlayPermission(mAppCtx)) {
                startClipboardService();
            } else {
                mPrefServActive.setChecked(false);
            }
        }
    }

    private void startClipboardService() {
        if (mAppCtx != null) {
            Intent serviceIntt = new Intent(mAppCtx, ClipboardListenerService.class);
            serviceIntt.setAction(ClipboardListenerService.ACTION_START);
            ContextCompat.startForegroundService(mAppCtx, serviceIntt);
        }
    }

    private void updateClipboardService(String action) {
        if (mAppCtx != null) {
            Intent serviceIntt = new Intent(mAppCtx, ClipboardListenerService.class);
            serviceIntt.setAction(action);
            ContextCompat.startForegroundService(mAppCtx, serviceIntt);
        }
    }

    private void stopClipboardService() {
        if (mAppCtx != null) {
            Intent serviceIntt = new Intent(mAppCtx, ClipboardListenerService.class);
            mAppCtx.stopService(serviceIntt);
        }
    }

    private void setContextMenuEnabled(boolean status) {
        try {
            PackageManager mPm = mAppCtx.getPackageManager();
            ComponentName mTranslActAlias = new ComponentName(mAppCtx, BuildConfig.APPLICATION_ID + ".TranslationDialogActivityAlias");   // apparently returns components that don't exist anymore
            mPm.setComponentEnabledSetting(mTranslActAlias, status ?
                                    PackageManager.COMPONENT_ENABLED_STATE_DEFAULT :    // set component to its default state (as specified in manifest)
                                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                        PackageManager.DONT_KILL_APP);  // android would kill app immediately if not set
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isContextMenuEnabled() {
        try {
            PackageManager mPm = mAppCtx.getPackageManager();
            ComponentName mTranslActAlias = new ComponentName(mAppCtx, BuildConfig.APPLICATION_ID + ".TranslationDialogActivityAlias");
            return mPm.getComponentEnabledSetting(mTranslActAlias) == PackageManager.COMPONENT_ENABLED_STATE_DEFAULT;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
