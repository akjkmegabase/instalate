/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.preference.ListPreference;

import com.concept1tech.unn.ArrayUtils;

public class ListPreferencePlus extends ListPreference {
    public ListPreferencePlus(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAttributes(context, attrs);
    }
    public ListPreferencePlus(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttributes(context, attrs);
    }
    public ListPreferencePlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributes(context, attrs);
    }
    public ListPreferencePlus(Context context) {
        super(context);
    }

    private void initAttributes(Context c, AttributeSet as) {
        TypedArray ta = c.obtainStyledAttributes(as, R.styleable.ListPreferencePlus);
        int sortOrder = ta.getInt(R.styleable.ListPreferencePlus_sortOrder, -1);
        ta.recycle();

        // sort entries
        if (sortOrder > 0) {
            switch (sortOrder) {
                case 1:
                case 2:
                    CharSequence[] entries = getEntries();
                    CharSequence[] entryVals = getEntryValues();

                    String[] entriesS = new String[entries.length];
                    int i = 0;
                    for (CharSequence ch : entries) {
                        entriesS[i++] = ch.toString();
                    }
                    Integer[] sortIdxs = ArrayUtils.getIdxsOfSorted(entriesS,
                                sortOrder == 1 ? ArrayUtils.ASCEND : ArrayUtils.DESCEND);

                    setEntries(ArrayUtils.getByIdxs(entries, sortIdxs));
                    setEntryValues(ArrayUtils.getByIdxs(entryVals, sortIdxs));
                    break;
                default:
                    break;
            }
        }
    }
}
