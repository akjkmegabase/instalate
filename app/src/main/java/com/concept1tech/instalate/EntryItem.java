/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.Context;

import com.concept1tech.unn.DroidUtils;

import java.util.ArrayList;

class EntryItem implements Comparable<EntryItem> {

    private final String mEntry;
    private final String mValue;
    private final Integer mIcon;
    private boolean mIsIconVisible = true;

    public EntryItem(String entry, String value, Integer icon) {
        this.mEntry = entry;
        this.mValue = value;
        this.mIcon = icon;
    }

    public static String[] getEntries(Context ctx, Integer resID) {
        return ctx.getResources().getStringArray(resID);
    }

    public static String[] getValues(Context ctx, Integer resID) {
        return ctx.getResources().getStringArray(resID);
    }

    public static int[] getIcons(Context ctx, Integer resID) {
        return DroidUtils.getResourcesArray(ctx, resID);
    }

    public static ArrayList<EntryItem> getAll(Context ctx, Integer entriesRes, Integer valuesRes, Integer iconsRes) {
        String[] entries = getEntries(ctx, entriesRes);
        String[] values = getValues(ctx, valuesRes);
        int[] icons = getIcons(ctx, iconsRes);
        ArrayList<EntryItem> entryItems = new ArrayList<>();
        for (int i = 0; i < entries.length; i++)
            entryItems.add(new EntryItem(entries[i], values[i], icons[i]));
        return entryItems;
    }

    // Getter
    public String getEntry() {
        return mEntry;
    }
    public String getValue() {
        return mValue;
    }
    public Integer getIcon() {
        return mIcon;
    }
    public boolean isIconVisible() {
        return mIsIconVisible;
    }
    public void setIconVisible(boolean isVisible) {
        mIsIconVisible = isVisible;
    }

    @Override
    public int compareTo(EntryItem entryItem) {
        return this.getEntry().compareToIgnoreCase(entryItem.getEntry());
    }
}
