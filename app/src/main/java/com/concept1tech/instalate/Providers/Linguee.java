/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate.Providers;

import android.content.Context;

import com.concept1tech.instalate.App;
import com.concept1tech.instalate.Provider;
import com.concept1tech.instalate.TranslationData;
import com.concept1tech.instalate.TranslationTask;
import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Linguee extends Provider {

    private final String mLabel;
    private final String mId;
    private final int mIcon;
    private final String[] mBaseUrls = {"https://www.linguee.com/%2$s-%3$s/search?source=auto&query=%1$s",
                "https://www.linguee.com/%2$s-%3$s/search?source=%2$s&query=%1$s",
                "https://www.linguee.com/%3$s-%2$s/search?source=%3$s&query=%1$s"};
    private final String[] mLangSpecs = {
                /*[0] ENGLISH*/          "english",
                /*[1] FRENCH*/           "french",
                /*[2] CZECH*/            "czech",
                /*[3] POLISH*/           "polish",
                /*[4] SLOVAK*/           "slovak",
                /*[5] GERMAN*/           "german",
                /*[6] HUNGARIAN*/        "hungarian",
                /*[7] DUTCH*/            "dutch",
                /*[8] ALBANIAN*/         null,
                /*[9] RUSSIAN*/          "russian",
                /*[10] SPANISH*/         "spanish",
                /*[11] SWEDISH*/         "swedish",
                /*[12] ICELANDIC*/       null,
                /*[13] NORWEGIAN*/       null,
                /*[14] ITALIAN*/         "italian",
                /*[15] FINNISH*/         "finnish",
                /*[16] DANISH*/          "danish",
                /*[17] PORTUGUESE*/      "portuguese",
                /*[18] CROATIAN*/        null,
                /*[19] BULGARIAN*/       "bulgarian",
                /*[20] ROMANIAN*/        "romanian",
                /*[21] LATIN*/           null,
                /*[22] ESPERANTO*/       null,
                /*[23] BOSNIAN*/         null,
                /*[24] TURKISH*/         null,
                /*[25] SERBIAN*/         null,
                /*[26] GREEK*/           "greek",
                /*[27] CHINESE*/         "chinese",
                /*[28] JAPANESE*/        "japanese",
                /*[29] SLOVENE*/         "slovene",
                /*[30] LITHUANIAN*/      "lithuanian",
                /*[31] LATVIAN*/         "latvian",
                /*[32] ESTONIAN*/        "estonian",
                /*[33] MALTESE*/         "maltese"
    };
    private final Boolean[][][] mCombinations;
    private final Integer[] mIsISO_8859_15 = {1, 5, 10, 17};  // indexes of languages which use ISO-8859-15 encoding, all others use UTF-8

    private final Pattern mPattern1 = Pattern.compile("class\\s*=\\s*['\"]\\s*shortened_begin2.*?>", Pattern.DOTALL);
    private final Pattern mPattern2 = Pattern.compile("class\\s*=\\s*['\"]\\s*shortened_end2.*?<", Pattern.DOTALL);
    private final Pattern mPattern3 = Pattern.compile("class\\s*=\\s*['\"]\\s*source_url.*?>", Pattern.DOTALL);
    private final Pattern mTrimWhitePattern = Pattern.compile("\\n\\s*");   // trim leading whitespace and remove newlines before re-parsing (removes unwanted spaces between chars)
    private final Pattern mRemovePlaceholderPattern = Pattern.compile("\\s*\\[\\.\\.\\.]\\s*");   // remove placeholder string [...]


    public Linguee(String label, String id, int icon) {
        mLabel = label;
        mId = id;
        mIcon = icon;

        mCombinations = new Boolean[3][][];
        mCombinations[0] = buildMat();
        mCombinations[1] = buildMat();
        mCombinations[2] = buildMat();

//        isTest();
    }

    @Override
    protected String getId() {
        return mId;
    }
    @Override
    protected Boolean[][][] getCombinations() {
        return mCombinations;
    }
    @Override
    protected void setUpPageRequest(Context c, PageRequest request, String s) {
        Integer[] props = ArrayUtils.parseInt(App.getActiveLangsFromSPrefs(c));
        setPageRequestDefaults(request);
        request.setCharset(getCharset(props));
        request.setUri(parseUri(mBaseUrls, s, mLangSpecs, props));
    }
    @Override
    protected void setUpTranslationData(Context c, TranslationData data) {
        List<ParcelableCharSequence> tList = data.getTranslationList();
        tList.clear();
        PageResponse response = data.getResponse();
        Document doc = Jsoup.parse(response.get());
        Elements cells = doc.select("table:first-child td");    // all cells of first table
        int i = 0;
        for (Element cell : cells) {
            String cellS = cell.outerHtml();

            int startIdx = 0;
            Matcher shortenedStart = mPattern1.matcher(cellS);
            if (shortenedStart.find())
                startIdx = shortenedStart.end();

            int endIdx = cellS.length() - 1;
            Matcher shortenedEnd = mPattern2.matcher(cellS);
            if (shortenedEnd.find())
                endIdx = shortenedEnd.end() - 1;
            else {
                Matcher urlMatch = mPattern3.matcher(cellS);
                if (urlMatch.find())
                    endIdx = urlMatch.end();
            }

            String sub = cellS.substring(startIdx, endIdx);
            sub = mTrimWhitePattern.matcher(sub).replaceAll("");
            sub = mRemovePlaceholderPattern.matcher(sub).replaceAll(" ");
            Document subDoc = Jsoup.parse(sub);
            String formatted = String.format(i % 2 == 0 ? "...%s..." : "       ...%s...\n", subDoc.text());
            tList.add(new ParcelableCharSequence(formatted));
            i++;
        }

        if (tList.size() == 0) {
            response.setError(TranslationTask.ERR_PARSING_RESPONSE);
        }
        response.set("");
        data.setProviderName(mLabel);
        data.setProviderIcon(mIcon);
    }

    private String getCharset(Integer[] curProp) {
        if (ArrayUtils.contains(mIsISO_8859_15, curProp[0]) ||
                    ArrayUtils.contains(mIsISO_8859_15, curProp[2])) {
            return "ISO-8859-15";
        }
        return "UTF-8";
    }

    private Boolean[][] buildMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);
        Boolean[] langSpecsB = ArrayUtils.toBool(mLangSpecs);

        Boolean[] reduced = langSpecsB.clone();
        reduced[27] = false; // -chinese
        reduced[9] = false; // -russian
        reduced[28] = false; // -japanese

        for (int i = 0; i < mat.length; i++) {
            if (i == 0) {
                ArrayUtils.setFromArray(mat, langSpecsB, 0, 0, 0);
            } else if (i >= 1 && i <= 7 ||
                        i >= 10 && i <= 11 ||
                        i >= 14 && i <= 17 ||
                        i >= 19 && i <= 20 ||
                        i == 26 ||
                        i >= 29 && i <= 33
            ) {
                ArrayUtils.setFromArray(mat, reduced, 0, i, 0);
            } else if (i == 27 || i == 9 || i == 28) {
                mat[i][0] = true;
            }
        }
        ArrayUtils.setMainDiagonal(mat, false);
        return mat;
    }

    private void isTest() {
        ArrayUtils.logMatrices(mCombinations, mLangSpecs, mLangSpecs, 4);
    }
}
