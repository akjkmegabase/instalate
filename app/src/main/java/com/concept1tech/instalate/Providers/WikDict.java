/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate.Providers;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.style.LeadingMarginSpan;
import android.text.style.StyleSpan;
import android.util.Log;

import com.concept1tech.instalate.App;
import com.concept1tech.instalate.Provider;
import com.concept1tech.instalate.R;
import com.concept1tech.instalate.TranslationData;
import com.concept1tech.instalate.TranslationTask;
import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.HTTPHeaders;
import com.concept1tech.unn.MySpannableStringBuilder;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class WikDict extends Provider {

    public static final String TAG = WikDict.class.getSimpleName();

    private final String mLabel;
    private final String mId;
    private final int mIcon;
    private final String[] mBaseUrls = {
                "https://www.wikdict.com/%2$s-%3$s/%1$s",         // bi-directional
                null,                                             // forward directional
                null};                                            // backwards directional

    // order is important
    private final String[] mLangSpecs = {
                /*[0] ENGLISH*/          "en",
                /*[1] FRENCH*/           "fr",
                /*[2] CZECH*/            null,
                /*[3] POLISH*/           "pl",
                /*[4] SLOVAK*/           null,
                /*[5] GERMAN*/           "de",
                /*[6] HUNGARIAN*/        null,
                /*[7] DUTCH*/            "nl",
                /*[8] ALBANIAN*/         null,
                /*[9] RUSSIAN*/          "ru",
                /*[10] SPANISH*/         "es",
                /*[11] SWEDISH*/         "sv",
                /*[12] ICELANDIC*/       null,
                /*[13] NORWEGIAN*/       "no",
                /*[14] ITALIAN*/         "it",
                /*[15] FINNISH*/         "fi",
                /*[16] DANISH*/          null,
                /*[17] PORTUGUESE*/      "pt",
                /*[18] CROATIAN*/        null,
                /*[19] BULGARIAN*/       "bg",
                /*[20] ROMANIAN*/        null,
                /*[21] LATIN*/           "la",
                /*[22] ESPERANTO*/       null,
                /*[23] BOSNIAN*/         null,
                /*[24] TURKISH*/         "tr",
                /*[25] SERBIAN*/         null,
                /*[26] GREEK*/           "el",
                /*[27] CHINESE*/         null,
                /*[28] JAPANESE*/        "ja",
                /*[29] SLOVENE*/         null,
                /*[30] LITHUANIAN*/      "lt",
                /*[31] LATVIAN*/         null,
                /*[32] ESTONIAN*/        null,
                /*[33] MALTESE*/         null,
                /*[34] MALAGASY*/        "mg",
                /*[35] INDONESIAN*/      "id"
    };
    private final Boolean[][][] mCombinations;    // 1st dimension: 1st element: matrix for bi-directional translation. 2nd element: matrix for forward-directional translation. 3nd element: matrix for backwards-directional translation

    public WikDict(String label, String id, int icon) {
        mLabel = label;
        mId = id;
        mIcon = icon;

        mCombinations = new Boolean[3][][];
        mCombinations[0] = buildBiDirMat();
        mCombinations[1] = buildForwardsMat();
        mCombinations[2] = buildBackwardsMat();

//        isTest();
    }

    @Override
    protected String getId() {
        return mId;
    }
    @Override
    protected Boolean[][][] getCombinations() {
        return mCombinations;
    }
    @Override
    protected void setUpPageRequest(Context c, PageRequest request, String s) {
        Integer[] props = ArrayUtils.parseInt(App.getActiveLangsFromSPrefs(c));
        setPageRequestDefaults(request);
        request.setHTTPHeaders(new HTTPHeaders("User-Agent", "InstaLate/1 (Android)"));      // identify ourselves as requested by WikDict webmaster
        request.setUri(parseUri(mBaseUrls, s, mLangSpecs, props));
    }
    @Override
    protected void setUpTranslationData(Context c, TranslationData data) {
        int leadingMargin = c.getResources().getDimensionPixelOffset(R.dimen.sp_32);
        List<ParcelableCharSequence> tList = data.getTranslationList();
        tList.clear();
        PageResponse response = data.getResponse();
        Document doc = Jsoup.parse(response.get());

        // (1) get "div" tag with attribute "id" that has value "results"
        // (2) get first "table" tag that is direct child from (1)
        // (3) get first "tbody" tag that is direct child from (2)
        // (4) get all "tr" tags that are direct child from (3)
        Elements tableRows = doc.select("div#results > table:first-of-type > tbody:first-of-type > tr");
        for (Element row : tableRows) {
            Elements cells = row.select("td");
            if (cells.size() < 2) {
                Log.w(TAG, "Row has less than 2 elements. Skipping...");
                continue;
            } else if (cells.size() > 2)
                Log.w(TAG, "Row has more than 2 elements");
            MySpannableStringBuilder builder = new MySpannableStringBuilder();
            for (int i = 0; i < 2; i++) {
                // left or right cell of table row
                Element curCell = cells.get(i);
                Elements transls = curCell.select("span.translation");
                int startIndent = i == 1 ? builder.length() : 0;
                for (Element transl : transls) {
                    // translation (term + gender + pronunciation)
                    Elements links = transl.getElementsByTag("a");
                    for (Element link : links) {
                        // term
                        builder.append(link.text());
                        builder.append(" ");
                    }
                    Elements genders = transl.select("span.gender");
                    for (Element gender : genders) {
                        // gender
                        builder.append(gender.text(), new StyleSpan(Typeface.ITALIC), SpannedString.SPAN_EXCLUSIVE_EXCLUSIVE);
                        builder.append(" ");
                    }
                    Elements pronuns = transl.select("span.pronun");
                    for (Element pronun : pronuns) {
                        // pronunciation
                        builder.append(String.format(Locale.US, "[%s] ", pronun.text()),
                                    new StyleSpan(Typeface.ITALIC), SpannedString.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    builder.append("\n");
                }
                Elements lis = curCell.select("ul.gloss li");
                for (Element li : lis) {
                    // gloss (bulleted)
                    builder.append(String.format(Locale.US, "∙ %s\n", li.text()),
                                new StyleSpan(Typeface.ITALIC), SpannedString.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                if (i == 1) {
                    builder.setSpan(new LeadingMarginSpan.Standard(leadingMargin), startIndent, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
            tList.add(new ParcelableCharSequence(builder));
        }


        if (tList.size() == 0) {
            response.setError(TranslationTask.ERR_PARSING_RESPONSE);
        }
        response.set("");
        data.setProviderName(mLabel);
        data.setProviderIcon(mIcon);
    }

    private Boolean[][] buildBiDirMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);

        Boolean[] en = ArrayUtils.toBool(mLangSpecs);
        ArrayUtils.setFromArray(mat, en, 0, 0, 0);           // en
        ArrayUtils.setFromArray(mat, en.clone(), 0, 5, 0);   // de
        ArrayUtils.setFromArray(mat, en.clone(), 0, 7, 0);   // nl
        ArrayUtils.setFromArray(mat, en.clone(), 0, 11, 0);  // sv
        ArrayUtils.setFromArray(mat, en.clone(), 0, 14, 0);  // it
        ArrayUtils.setFromArray(mat, en.clone(), 0, 15, 0);  // fi
        ArrayUtils.setFromArray(mat, en.clone(), 0, 17, 0);  // pt

        Boolean[] pl = en.clone();
        pl[34] = false;
        ArrayUtils.setFromArray(mat, pl, 0, 3, 0);           // pl
        ArrayUtils.setFromArray(mat, pl.clone(), 0, 1, 0);   // fr

        Boolean[] ru = en.clone();
        ru[9] = ru[10] = ru[13] = ru[19] = ru[21] = ru[24] = ru[26] = ru[30] = ru[35] = false;
        ArrayUtils.setFromArray(mat, ru, 0, 9, 0);           // ru
        ArrayUtils.setFromArray(mat, ru.clone(), 0, 10, 0);  // es

        Boolean[] ja = en.clone();
        ja[13] = ja[21] = ja[24] = ja[26] = ja[28] = ja[35] = false;
        ArrayUtils.setFromArray(mat, ja, 0, 28, 0);          // ja

        Boolean[] bg = new Boolean[mLangSpecs.length];
        Arrays.fill(bg, false);
        bg[0] = bg[1] = bg[3] = bg[5] = bg[7] = bg[11] = bg[14] = bg[15] = bg[17] = bg[28] = true;
        ArrayUtils.setFromArray(mat, bg, 0, 19, 0);          // bg
        ArrayUtils.setFromArray(mat, bg.clone(), 0, 30, 0);  // lt

        Boolean[] mg = new Boolean[mLangSpecs.length];
        Arrays.fill(mg, false);
        mg[0] = mg[5] = mg[7] = mg[9] = mg[10] = mg[11] = mg[14] = mg[15] = mg[17] = mg[28] = true;
        ArrayUtils.setFromArray(mat, mg, 0, 34, 0);          // mg

        Boolean[] no = new Boolean[mLangSpecs.length];
        Arrays.fill(no, false);
        no[0] = no[1] = no[3] = no[5] = no[7] = no[11] = no[14] = no[15] = no[17] = true;
        ArrayUtils.setFromArray(mat, no, 0, 13, 0);          // no
        ArrayUtils.setFromArray(mat, no.clone(), 0, 21, 0);  // la
        ArrayUtils.setFromArray(mat, no.clone(), 0, 24, 0);  // tr
        ArrayUtils.setFromArray(mat, no.clone(), 0, 26, 0);  // el
        ArrayUtils.setFromArray(mat, no.clone(), 0, 35, 0);  // id

        ArrayUtils.setMainDiagonal(mat, false);
        return mat;
    }

    private Boolean[][] buildForwardsMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);
        return mat;
    }

    private Boolean[][] buildBackwardsMat() {
        return buildForwardsMat();
    }

    private void isTest() {
        ArrayUtils.logMatrices(mCombinations, mLangSpecs, mLangSpecs, 4);
    }
}

