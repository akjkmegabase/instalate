/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate.Providers;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spanned;
import android.text.style.LeadingMarginSpan;
import android.text.style.StyleSpan;

import com.concept1tech.instalate.App;
import com.concept1tech.instalate.Provider;
import com.concept1tech.instalate.R;
import com.concept1tech.instalate.TranslationData;
import com.concept1tech.instalate.TranslationTask;
import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.MySpannableStringBuilder;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class Gcide extends Provider {

    private final String mLabel;
    private final String mId;
    private final int mIcon;
    private final String[] mBaseUrls = {
                "https://gcide.gnu.org.ua/?q=%1$s&define=Define&strategy=.",
                "",
                "",
    };

    private final String[] mLangSpecs = {"en"};
    private final Boolean[][][] mCombinations;

    private final Pattern mSplitParagraphsPattern = Pattern.compile("\\n+(?=\\d+\\. )");   // split at "\n1. ", "\n2. " etc. (positive lookahead)
    private final Pattern mInlineQuotesPattern = Pattern.compile("\\n+\\[");    // move quotes to same line
    private final Pattern mSplitSynsPattern = Pattern.compile("\\n+(?=Syn\\. -- )");   // some numbered paragraphs (many last ones) have a "Syn. -- " section, split it off
    private final Pattern mSplitGlossNQuotesPattern = Pattern.compile("\\n\\n");


    public Gcide(String label, String id, int icon) {
        mLabel = label;
        mId = id;
        mIcon = icon;

        mCombinations = new Boolean[3][1][1];
        mCombinations[0][0][0] = true;
        mCombinations[1][0][0] = false;
        mCombinations[2][0][0] = false;
    }


    @Override
    protected String getId() {
        return mId;
    }
    @Override
    protected Boolean[][][] getCombinations() {
        return mCombinations;
    }
    @Override
    protected void setUpPageRequest(Context c, PageRequest request, String s) {
        Integer[] props = ArrayUtils.parseInt(App.getActiveLangsFromSPrefs(c));
        setPageRequestDefaults(request);
        request.setUri(parseUri(mBaseUrls, s, mLangSpecs, props));
    }
    @Override
    protected void setUpTranslationData(Context c, TranslationData data) {
        // some searches that illustrate the variability of gcide.gnu.org.ua's formatting:
        // single paragraph: "conceal", "wrap"
        // with syn paragraph: "knowledge", "field, "conceal", "point"
        // unnumbered paragraph: "view"

        int leadingMargin = c.getResources().getDimensionPixelOffset(R.dimen.sp_32);
        List<ParcelableCharSequence> tList = data.getTranslationList();
        tList.clear();
        PageResponse response = data.getResponse();
        Document doc = Jsoup.parse(response.get());

        // (1) get "div" tag with attribute "id" that has value "results"
        // (2) get "ol" tag with attribute "class" that has value "definitions" and is a descendent of (1)
        Elements definitions = doc.select("div#results ol[class=definitions] li");
        int i = 1;
        for (Element definition : definitions) {
            // preserve linebreaks (convert <br>/</br> to "\n")
            String textNBreaks = Jsoup.clean(definition.html(), "", Whitelist.none().addTags("br"),
                        new Document.OutputSettings().prettyPrint(true));
            String text = Jsoup.clean(textNBreaks, "", Whitelist.none(),
                        new Document.OutputSettings().prettyPrint(false));      // disable pretty print to retain breaks


            String[] numberedParagraphs = mSplitParagraphsPattern.split(text);
            boolean isSingleParagraph = numberedParagraphs.length == 1;
            int j = 0;
            for (String numberedP : numberedParagraphs) {
                MySpannableStringBuilder builder = new MySpannableStringBuilder();
                numberedP = mInlineQuotesPattern.matcher(numberedP).replaceAll(" [");
                if (j == 0) {
                    builder.append(String.format(Locale.US, "Definition %d:\n", i++), new StyleSpan(Typeface.BOLD), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                if (j == 0 && /*fixes "conceal"*/ !isSingleParagraph) {
                    // definition
                    builder.append(numberedP).append("\n");
                } else {
                    // gloss with quotes (or synonyms (with quotes))
                    String[] glossAndSyn = mSplitSynsPattern.split(numberedP);
                    int k = 0;
                    for (String glossOrSyn : glossAndSyn) {
                        if (k++ >= 1) {
                            // synonym
                            builder.append("\n");
                        }
                        String[] glossNQuotes = mSplitGlossNQuotesPattern.split(glossOrSyn);
                        int l = 0;
                        for (String item : glossNQuotes) {
                            if (l++ == 0) {
                                // gloss
                                builder.append(item);
                            } else {
                                // quotes/others
                                int start = builder.length();
                                builder.append("∙ ").append(item);  // android bullet spans look awful and bullets don't resize
                                builder.setSpan(new LeadingMarginSpan.Standard(leadingMargin), start, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                builder.setSpan(new StyleSpan(Typeface.ITALIC), start, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            }
                            builder.append("\n");
                        }
                    }
                }
                tList.add(new ParcelableCharSequence(builder));
                j++;
            }
        }

        if (tList.size() == 0) {
            response.setError(TranslationTask.ERR_PARSING_RESPONSE);
        }
        response.set("");
        data.setProviderName(mLabel);
        data.setProviderIcon(mIcon);
    }
}
