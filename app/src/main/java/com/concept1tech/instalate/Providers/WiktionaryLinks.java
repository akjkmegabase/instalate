/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate.Providers;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.LeadingMarginSpan;
import android.text.style.StyleSpan;

import com.concept1tech.instalate.App;
import com.concept1tech.instalate.Provider;
import com.concept1tech.instalate.R;
import com.concept1tech.instalate.TranslationData;
import com.concept1tech.instalate.TranslationTask;
import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class WiktionaryLinks extends Provider {

    private final String mLabel;
    private final String mId;
    private final int mIcon;
    private final String mSep = "_";  // replaces whitespace in search query
    private final String[] mBaseUrls = {
                null,      // bi-directional
                "https://%2$s.wiktionary.org/w/api.php?action=query&prop=iwlinks&format=json&iwlimit=30&iwprefix=%3$s&titles=%1$s",        // forward directional
                "https://%3$s.wiktionary.org/w/api.php?action=query&prop=iwlinks&format=json&iwlimit=30&iwprefix=%2$s&titles=%1$s"       // backwards directional
    };
    private final String[] mBrowserLinks = {
                null,
                "https://%2$s.wiktionary.org/wiki/%1$s",
                "https://%3$s.wiktionary.org/wiki/%1$s"
    };
    private Uri mCurBrowserLink = Uri.parse("");

    // order is important
    private final String[] mLangSpecs = {
                /*[0] ENGLISH*/          "en",
                /*[1] FRENCH*/           "fr",
                /*[2] CZECH*/            null,
                /*[3] POLISH*/           null,
                /*[4] SLOVAK*/           null,
                /*[5] GERMAN*/           "de",
                /*[6] HUNGARIAN*/        null,
                /*[7] DUTCH*/            null,
                /*[8] ALBANIAN*/         null,
                /*[9] RUSSIAN*/          null,
                /*[10] SPANISH*/         null,
                /*[11] SWEDISH*/         null,
                /*[12] ICELANDIC*/       null,
                /*[13] NORWEGIAN*/       null,
                /*[14] ITALIAN*/         null,
                /*[15] FINNISH*/         null,
                /*[16] DANISH*/          null,
                /*[17] PORTUGUESE*/      null,
                /*[18] CROATIAN*/        null,
                /*[19] BULGARIAN*/       null,
                /*[20] ROMANIAN*/        null,
                /*[21] LATIN*/           null,
                /*[22] ESPERANTO*/       null,
                /*[23] BOSNIAN*/         null,
                /*[24] TURKISH*/         null,
                /*[25] SERBIAN*/         null,
                /*[26] GREEK*/           null,
                /*[27] CHINESE*/         null,
                /*[28] JAPANESE*/        null,
                /*[29] SLOVENE*/         null,
                /*[30] LITHUANIAN*/      null,
                /*[31] LATVIAN*/         null,
                /*[32] ESTONIAN*/        null,
                /*[33] MALTESE*/         null
    };
    private final Boolean[][][] mCombinations;    // 1st dimension: 1st element: matrix for bi-directional translation. 2nd element: matrix for forward-directional translation. 3nd element: matrix for backwards-directional translation

    public WiktionaryLinks(String label, String id, int icon) {
        mLabel = label;
        mId = id;
        mIcon = icon;

        mCombinations = new Boolean[3][][];
        mCombinations[0] = buildBiDirMat();
        mCombinations[1] = buildForwardsMat();
        mCombinations[2] = buildBackwardsMat();

//        isTest();
    }

    @Override
    protected String getId() {
        return mId;
    }
    @Override
    protected Boolean[][][] getCombinations() {
        return mCombinations;
    }
    @Override
    protected void setUpPageRequest(Context c, PageRequest request, String s) {
        s = s.replaceAll("\\s+", mSep);
        Integer[] props = ArrayUtils.parseInt(App.getActiveLangsFromSPrefs(c));
        setPageRequestDefaults(request);
        request.setUri(parseUri(mBaseUrls, s, mLangSpecs, props));
        mCurBrowserLink = parseUri(mBrowserLinks, s, mLangSpecs, props);
    }
    @Override
    protected void setUpTranslationData(Context c, TranslationData data) {
        int leadingMargin = c.getResources().getDimensionPixelOffset(R.dimen.sp_32);
        List<ParcelableCharSequence> tList = data.getTranslationList();
        tList.clear();
        PageResponse response = data.getResponse();

        try {
            JSONObject root = new JSONObject(response.get());
            JSONObject pages = root.getJSONObject("query").getJSONObject("pages");
            String id = pages.keys().next();
            if (!id.equals("-1")) {
                JSONObject page = pages.getJSONObject(id);
                if (page.has("iwlinks")) {
                    SpannableString title = new SpannableString(String.format("%s:", page.optString("title")));
                    title.setSpan(new StyleSpan(Typeface.BOLD), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tList.add(new ParcelableCharSequence(title));
                    JSONArray linksArr = page.getJSONArray("iwlinks");
                    for (int i = 0; i < linksArr.length(); i++) {
                        JSONObject curL = linksArr.getJSONObject(i);
                        String transl = curL.optString("*");
                        transl = transl.startsWith("Special:Search/") ? transl.replace("Special:Search/", "") : transl;
                        SpannableString translSS = new SpannableString(String.format("∙ %s", transl));
                        translSS.setSpan(new LeadingMarginSpan.Standard(leadingMargin), 0, translSS.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tList.add(new ParcelableCharSequence(translSS));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (tList.size() == 0) {
            response.setError(TranslationTask.ERR_PARSING_RESPONSE);
        }
        response.set("");
        data.setProviderName(mLabel);
        data.setProviderIcon(mIcon);
        data.setBrowserLink(mCurBrowserLink);
    }

    private Boolean[][] buildBiDirMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);
        return mat;
    }

    private Boolean[][] buildForwardsMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);

        Boolean[] langSpecsB = ArrayUtils.toBool(mLangSpecs);
        for (int i = 0; i < langSpecsB.length; i++) {
            if (langSpecsB[i]) {
                ArrayUtils.setFromArray(mat, langSpecsB.clone(), 0, i, 0);
            }
        }

        ArrayUtils.setMainDiagonal(mat, false);
        return mat;
    }

    private Boolean[][] buildBackwardsMat() {
        return buildForwardsMat();
    }

    private void isTest() {
        ArrayUtils.logMatrices(mCombinations, mLangSpecs, mLangSpecs, 4);
    }
}
