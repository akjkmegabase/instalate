/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.concept1tech.unn.DroidUtils;
import com.concept1tech.unn.NetworkUtils;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;

import java.lang.ref.WeakReference;

public class TranslationTask extends AsyncTask<PageRequest, Void, PageResponse> {

    public static final String ERR_INVALID_SEARCH_QUERY = "ERR_INVALID_SEARCH_QUERY";
    public static final String ERR_PARSING_RESPONSE = "ERR_PARSING_RESPONSE";
    private static TranslationData sOldData;

    private Provider mCurProvider;
    private final WeakReference<Context> mACtx;      // shouldnt leak because a) weak ref b) we use app context
    private final String mOrigClip;
    private TranslationData mData;

    TranslationTask(TranslationDialogActivity translationActivity, String origClip) {
        mACtx = new WeakReference<>(translationActivity.getApplicationContext());
        mOrigClip = origClip;
        translationActivity.setOnCancelListener(() -> {
            if (!isCancelled())
                cancel(true);
        });
    }

    protected void executeTranslation() {
        if (DroidUtils.isNonNullWeakRef(mACtx)) {
            mData = new TranslationData(mOrigClip);
            Context ctx = mACtx.get();
            if (hasInternetConnection(ctx)) {
                String clipStr = preProcess(mOrigClip);
                if (clipStr.isEmpty()) {
                    mData.getResponse().setError(ERR_INVALID_SEARCH_QUERY);
                    showActivity(mData);
                } else {
                    mCurProvider = Provider.getCurrent(ctx);
                    mCurProvider.setUpPageRequest(ctx, mData.getRequest(), clipStr);
                    if (sOldData != null && sOldData.getRequest().equals(mData.getRequest())) {
                        showActivity(sOldData);
                    } else {
                        execute(mData.getRequest());
                    }
                }
            } else {
                mData.getResponse().setError(NetworkUtils.ERR_NO_NETWORK);
                showActivity(mData);
            }
        }
    }

    private String preProcess(String s) {
        s = s.replaceAll("[^\\p{L}\\s]+", " ");  // replace all chars that are not letters (from any language) or whitespace (see: https://regex101.com/r/lP8pG9/156) with space
        s = s.trim();
        return s;
    }


    // async worker
    @Override
    protected PageResponse doInBackground(PageRequest... pageRequests) {
        return NetworkUtils.getPageSource(pageRequests[0]);
    }
    @Override
    protected void onPostExecute(PageResponse response) {
        super.onPostExecute(response);
        if (DroidUtils.isNonNullWeakRef(mACtx)) {
            mData.setResponse(response);
            if (!response.hasErrors()) {
                mCurProvider.setUpTranslationData(mACtx.get(), mData);
                sOldData = mData;
            }
            showActivity(mData);
        }
    }


    void showActivity(TranslationData data) {
        if (DroidUtils.isNonNullWeakRef(mACtx)) {
            Context ctx = mACtx.get();
            Intent intt = TranslationDialogActivity.getBaseIntent(ctx);
            intt.putExtra(App.TRANSLATION_DATA_EXTRA, data);
            ctx.startActivity(intt);
        }
    }

    // wrapper method for testing reasons
    boolean hasInternetConnection(Context c) {
        return NetworkUtils.isConnected(c);
    }
}
