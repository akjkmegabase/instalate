/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.concept1tech.unn.NetworkUtils;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import java.util.List;

public class TranslationDialogActivity extends AppCompatActivity {

    public static final String TAG = TranslationDialogActivity.class.getSimpleName();

    public interface OnCancelListener {
        void onCancel();
    }

    private boolean needToGetSelectedText = false;
    private RecyclerView mTranslationRv;
    private TranslationAdapter mTranslationAdapter;
    private Uri mBrowserLink = Uri.parse("");
    private ClipboardManager mClipboardManager;
    private ProgressBar mProgressBar;
    private Group mProviderGroup;
    private TextView mTvProviderName;
    private ImageView mIvProviderIcon;
    private OnCancelListener mOnCancelListener;
    private ConstraintLayout mConstraintLayout;
    private Intent mCurIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation_dialog);
        mTranslationRv = findViewById(R.id.rv_translation_dialog);
        mClipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        mProgressBar = findViewById(R.id.progressBar);
        mProviderGroup = findViewById(R.id.group_provider);
        mTvProviderName = findViewById(R.id.tv_provider);
        mIvProviderIcon = findViewById(R.id.iv_provider_icon);
        mConstraintLayout = findViewById(R.id.cl_root);

        SharedPreferences defaultSPref = PreferenceManager.getDefaultSharedPreferences(this);
        String fs = defaultSPref.getString("prefkey_fontsize_translation", "14");

        mTranslationAdapter = new TranslationAdapter(this, Float.parseFloat(fs));
        mTranslationRv.setAdapter(mTranslationAdapter);
        mTranslationRv.setLayoutManager(new LinearLayoutManager(this));

        mCurIntent = getIntent();
        setActivityUp();
    }


    public static Intent getBaseIntent(Context ctx) {
        Intent intt = new Intent(ctx, TranslationDialogActivity.class);
        return intt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);     // Calling startActivity() from outside of an Activity context requires FLAG_ACTIVITY_NEW_TASK
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mCurIntent = intent;
        setActivityUp();
        mTranslationRv.setScrollY(0);
    }

    private void translateText() {
        String selectedText = "";
        // triggered by context menu item selection
        if (mCurIntent.hasExtra(Intent.EXTRA_PROCESS_TEXT)) {
            CharSequence tmp = mCurIntent.getCharSequenceExtra(Intent.EXTRA_PROCESS_TEXT);
            if (tmp != null) {
                selectedText = tmp.toString();
            }
        }
        // triggered by clipboard changes
        else {
            selectedText = ClipboardListenerService.getTextItemInClipboard(mClipboardManager);
        }

        // calling TranslationTask here is imperative to disable the loading screen
        if (selectedText == null) selectedText = "";
        (new TranslationTask(this, selectedText)).executeTranslation();
        needToGetSelectedText = false;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        // needToGetSelectedText flag prevents unnecessary reading the clipboard (or accessing Intent.EXTRA_PROCESS_TEXT) on every focus change (for example when pulling down the status bar panel)
        // callback order: onCreate--onWindowFocusChanged--onNewIntent (if activity already created: onNewIntent--onWindowFocusChanged--onNewIntent)
        if (needToGetSelectedText && hasFocus) {
            translateText();
        }
    }

    private void setActivityUp() {
        if (mCurIntent.hasExtra(App.TRANSLATION_DATA_EXTRA)) {
            mProgressBar.setVisibility(View.GONE);
            mTranslationRv.setVisibility(View.VISIBLE);
            TranslationData data = mCurIntent.getParcelableExtra(App.TRANSLATION_DATA_EXTRA);
            if (data != null) {
                int providerGroupVisi = View.GONE;
                List<ParcelableCharSequence> items = data.getTranslationList();
                PageResponse response = data.getResponse();
                mBrowserLink = data.getBrowserLink();
                if (response.hasErrors()) {
                    String errText = "";
                    // errors are sorted by priority
                    if (response.hasError(TranslationTask.ERR_INVALID_SEARCH_QUERY)) {
                        errText = String.format("%s: \"%s\"", getString(R.string.invalid_search_term), data.getOrigClip());
                    } else {
                        String requestUrl = data.getRequest().getUri().toString();
                        if (response.hasError(NetworkUtils.ERR_NO_NETWORK)) {
                            errText = String.format("%s", getString(R.string.network_error));
                        } else if (response.hasError(NetworkUtils.ERR_CONNECTION_TIMEOUT)) {
                            errText = String.format("%s\n\n%s: %s \n", getString(R.string.timeout_error), getString(R.string.url), requestUrl);
                        } else if (response.hasError(NetworkUtils.ERR_NO_CONNECTION)) {
                            errText = String.format("%s\n\n%s: %s \n", getString(R.string.connection_error), getString(R.string.url), requestUrl);
                        } else if (response.hasError(NetworkUtils.ERR_NO_DATA)) {
                            errText = String.format("%s\n\n%s: %s \n", getString(R.string.no_data_error), getString(R.string.url), requestUrl);
                        } else if (response.hasError(TranslationTask.ERR_PARSING_RESPONSE)) {
                            errText = String.format("%s\n\n%s: %s \n", getString(R.string.no_results_error), getString(R.string.url), requestUrl);
                        }
                    }
                    items.clear();
                    items.add(new ParcelableCharSequence(errText));
                } else {
                    providerGroupVisi = View.VISIBLE;
                    String shortName = data.getProviderName().replaceAll("\\s*\\(.*", "");   // strip everything after "(" from name
                    mTvProviderName.setText(shortName);
                    mIvProviderIcon.setImageResource(data.getProviderIcon());
                }
                mTranslationAdapter.setTranslationItems(items);
                mProviderGroup.setVisibility(providerGroupVisi);
                if (providerGroupVisi == View.VISIBLE) {
                    mProviderGroup.updatePreLayout(mConstraintLayout);      // fixes bug androidx.constraintlayout.widget.Group randomly not showing up if set to View.VISIBLE for Linguee provider (see: https://github.com/android/views-widgets-samples/issues/67)
                }
            }
        } else {
            needToGetSelectedText = true;
            mProgressBar.setVisibility(View.VISIBLE);
            mTranslationRv.setVisibility(View.GONE);
            mProviderGroup.setVisibility(View.GONE);

            // if window has already focus, i.e. we want to translate a word displayed in TranslationDialogActivity onWindowFocusChanged() (and translateText()) won't get called
            if (hasWindowFocus())
                translateText();
        }
    }

    public void openWebPage(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, mBrowserLink);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.w(TAG, "Can't handle implicit intent to open webpage " + mBrowserLink.toString());
        }
    }
    public void onCloseButtonClick(View view) {
        finish();
    }
    public void onSettingsButtonClick(View view) {
        startActivity(new Intent(this, PrefsActivity.class));
    }


    private void cancelTranslationTask() {
        if (mOnCancelListener != null) {
            mOnCancelListener.onCancel();
            mOnCancelListener = null;     // remove any reference to TranslationTask/OnCancelListener so that it can be garbage-collected
        }
    }
    @Override
    public void onStop() {      // cancel task if activity is not visible anymore (but don't if activity is not in foreground but still visible)
        super.onStop();
        cancelTranslationTask();
        if (!isFinishing())
            finish();       // if we cancelTranslationTask we need to finish to remove loading screen too, also this auto-closes the translation if moved to the background
    }
    public void setOnCancelListener(OnCancelListener listener) {
        cancelTranslationTask();    // if we already listen for cancels for another TranslationTask cancel it
        mOnCancelListener = listener;
    }
}
