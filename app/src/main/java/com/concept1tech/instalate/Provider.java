/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;

import com.concept1tech.instalate.Providers.Beolingus;
import com.concept1tech.instalate.Providers.Dictcc;
import com.concept1tech.instalate.Providers.Gcide;
import com.concept1tech.instalate.Providers.Linguee;
import com.concept1tech.instalate.Providers.WikDict;
import com.concept1tech.instalate.Providers.WiktionaryLinks;
import com.concept1tech.unn.DroidUtils;
import com.concept1tech.unn.HTTPHeaders;
import com.concept1tech.unn.PageRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;

public abstract class Provider {

    private static HashMap<String, Provider> sProviders;

    protected abstract String getId();

    protected abstract Boolean[][][] getCombinations();

    protected abstract void setUpPageRequest(Context c, PageRequest request, String s);

    protected abstract void setUpTranslationData(Context c, TranslationData data);


    public static HashMap<String, Provider> getAll(Context ctx) {
        if (sProviders == null) {
            sProviders = new HashMap<>();
            Resources res = ctx.getResources();
            String[] proNames = res.getStringArray(R.array.pro_names);
            String[] proValues = res.getStringArray(R.array.pro_names_values);
            int[] proIcons = DroidUtils.getResourcesArray(ctx, R.array.pro_favicons);
            sProviders.put(proValues[0], new WikDict(proNames[0], proValues[0], proIcons[0]));
            sProviders.put(proValues[1], new Dictcc(proNames[1], proValues[1], proIcons[1]));
            sProviders.put(proValues[2], new Linguee(proNames[2], proValues[2], proIcons[2]));
            sProviders.put(proValues[3], new Beolingus(proNames[3], proValues[3], proIcons[3]));
            sProviders.put(proValues[4], new WiktionaryLinks(proNames[4], proValues[4], proIcons[4]));
            sProviders.put(proValues[5], new Gcide(proNames[5], proValues[5], proIcons[5]));
        }
        return sProviders;
    }

    public static Provider getCurrent(Context ctx) {
        HashMap<String, Provider> providers = getAll(ctx);
        return providers.get(App.getCurProviderFromSPrefs(ctx));
    }

    protected Uri parseUri(String[] bases, String s, String[] langs, Integer[] props) {
        try {
            s = URLEncoder.encode(s, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Uri.parse(String.format(Locale.US, bases[props[1]], s, langs[props[0]], langs[props[2]]));
    }

    protected void setPageRequestDefaults(PageRequest request) {
        request.setCharset("UTF-8");
        request.setHTTPHeaders(HTTPHeaders.getPreset(HTTPHeaders.FIREFOX_85_WIN_US));
    }
}
