/*
 InstaLate (instant translation app)
 Copyright (C) 2020 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.Context;

import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.PageRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.robolectric.ParameterizedRobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.AdditionalMatchers.aryEq;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

// Parameterized tests for PrefsRootFragment.setLanguages()
@RunWith(ParameterizedRobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class SetLanguagesTest {

    private static final HashMap<String, Provider> mProviders = getProviders();    // use simple custom provider for test
    private static final ArrayList<EntryItem> mAllLanguages = getLangsEntryItems();   // unable to get access to resources with Robolectric on API 29 until Android supports Java 9
    private static final ArrayList<EntryItem> mAllDirections = getDirectsEntryItems();

    // create test data
    @ParameterizedRobolectricTestRunner.Parameters
    public static Collection<Object[]> data() {
        @SuppressWarnings("unchecked")
        List<EntryItem>[] expectedEntries = new List[3];
        expectedEntries[0] = new LinkedList<>();
        expectedEntries[1] = new LinkedList<>();
        expectedEntries[2] = new LinkedList<>();
        expectedEntries[0].add(mAllLanguages.get(1));
        expectedEntries[1].add(mAllDirections.get(0));
        expectedEntries[2].add(mAllLanguages.get(2));

        String[] expectedProps = new String[]{"1", "0", "2"};

        Object[][] data = new Object[][]{{
                    new String[]{"1", "0", "2"},    // current active props
                    expectedProps,                  // expected props after validateActiveProps() (for now constant for all tests)
                    expectedEntries                 // expected EntryItems (for now constant for all tests)
        }, {
                    new String[]{"1", "0", "17"},
                    expectedProps,
                    expectedEntries
        }, {
                    new String[]{"17", "0", "2"},
                    expectedProps,
                    expectedEntries
        }, {
                    new String[]{"1", "2", "2"},
                    expectedProps,
                    expectedEntries
        }, {
                    new String[]{"17", "0", "17"},
                    expectedProps,
                    expectedEntries
        }, {
                    new String[]{"17", "2", "17"},
                    expectedProps,
                    expectedEntries
        }};
        return Arrays.asList(data);
    }


    private final MockedStatic.Verification mGetAllIsCalled = () -> Provider.getAll(any());
    private final MockedStatic.Verification mGetAllIsCalled2 = () -> EntryItem.getAll(any(), anyInt(), anyInt(), anyInt());
    private AutoCloseable mCloseable;

    // Parameters
    private final String[] mCurrentProps;
    private final String[] mExpectedProps;
    private final List<EntryItem>[] mExpectedEntries;

    public SetLanguagesTest(String[] currentProps, String[] expectedProps, List<EntryItem>[] expectedEntries) {
        mCurrentProps = currentProps;
        mExpectedProps = expectedProps;
        mExpectedEntries = expectedEntries;
    }

    @Mock
    private TripleListPreference mLangsDirectsPrefMock;

    // inject mocks FragmentActivity + TripleListPreference into PrefsRootFragment
    @InjectMocks
    private final PrefsRootFragment mFragSpy = spy(new PrefsRootFragment());    // spy into PrefsRootFragment to stub some methods

    @Before
    public void initMocks() {
        // with openMocks() instead of @RunWith(MockitoJUnitRunner.class) we're able to use another runner, RobolectricTestRunner in this case
        mCloseable = MockitoAnnotations.openMocks(this);
    }
    @After
    public void after() throws Exception {
        mCloseable.close();
    }


    @Test
    public void setLanguagesShouldSwitchToValidLanguageEntries() {
        doReturn("some string").when(mFragSpy).getString(anyInt());
        doNothing().when(mFragSpy).notifyCombinationChange(any());

        try (MockedStatic<Provider> providerMock = mockStatic(Provider.class);
             MockedStatic<EntryItem> entryItemMock = mockStatic(EntryItem.class)
        ) {
            providerMock.when(mGetAllIsCalled).thenReturn(mProviders);
            entryItemMock.when(mGetAllIsCalled2).
                        thenReturn(mAllLanguages).
                        thenReturn(mAllDirections);

            // execute
            mFragSpy.setLanguages("testprovider", mCurrentProps);
        }

        verify(mLangsDirectsPrefMock).setActiveValues(aryEq(mExpectedProps));    // could use ArgumentCaptor here too
        verify(mLangsDirectsPrefMock).setEntryItems(aryEq(mExpectedEntries));
    }


    private static HashMap<String, Provider> getProviders() {
        HashMap<String, Provider> provs = new HashMap<>();
        Provider testProvider = new TestProvider();
        provs.put(testProvider.getId(), testProvider);
        return provs;
    }

    private static ArrayList<EntryItem> getLangsEntryItems() {
        ArrayList<EntryItem> list = new ArrayList<>();
        list.add(new EntryItem("language 0", "", 0));
        list.add(new EntryItem("language 1", "", 0));
        list.add(new EntryItem("language 2", "", 0));
        list.add(new EntryItem("language 3", "", 0));   // note that list.size() > mat.length (or mat[0].length)
        return list;
    }

    private static ArrayList<EntryItem> getDirectsEntryItems() {
        ArrayList<EntryItem> list = new ArrayList<>();
        list.add(new EntryItem("direction 0", "", 0));
        list.add(new EntryItem("direction 1", "", 0));
        list.add(new EntryItem("direction 2", "", 0));
        return list;
    }

    private static class TestProvider extends Provider {

        @Override
        protected String getId() {
            return "testprovider";
        }
        @Override
        protected Boolean[][][] getCombinations() {
            Boolean[][][] mat3d = new Boolean[3][][];
            Boolean[][] mat = new Boolean[3][3];
            ArrayUtils.fill(mat, false);
            mat[1][2] = true;
            // mat:
            //    l0 l1 l2
            // l0 .  .  .
            // l1 .  .  X
            // l2 .  .  .
            mat3d[0] = mat;
            mat3d[1] = new Boolean[3][3];
            ArrayUtils.fill(mat3d[1], false);
            mat3d[2] = mat3d[1];
            return mat3d;
        }
        @Override
        protected void setUpPageRequest(Context c, PageRequest request, String s) {
        }
        @Override
        protected void setUpTranslationData(Context c, TranslationData data) {
        }
    }
}
